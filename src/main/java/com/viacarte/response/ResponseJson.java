package com.viacarte.response;

import org.springframework.http.HttpStatus;

public class ResponseJson {

	private String message;

	private HttpStatus code;
	
	private String sessionId;

	private boolean success;

	private Object details;

	public String getMessage() {
		return message;
	}

	
	
	public String getSessionId() {
		return sessionId;
	}



	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}



	public void setMessage(String message) {
		this.message = message;
	}

	public HttpStatus getCode() {
		return code;
	}

	public void setCode(HttpStatus code) {
		this.code = code;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

}
