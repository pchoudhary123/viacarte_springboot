package com.viacarte.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.viacarte.entity.KycDetail;
import com.viacarte.entity.User;

public interface KycRepository extends CrudRepository<KycDetail, Long>, PagingAndSortingRepository<KycDetail, Long>,
		JpaSpecificationExecutor<KycDetail> {



}
