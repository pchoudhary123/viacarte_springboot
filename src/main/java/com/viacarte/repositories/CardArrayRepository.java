package com.viacarte.repositories;

import com.viacarte.entity.CardArray;
import com.viacarte.entity.Issuer;
import com.viacarte.entity.User;
import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CardArrayRepository extends CrudRepository<CardArray, Long>,
        PagingAndSortingRepository<CardArray, Long>, JpaSpecificationExecutor<CardArray> {

    CardArray findByCardName(String cardName);

    List<CardArray> findAllByStatus(Status active);

    List<CardArray> findByIdIn(List<Long> ids);

}
