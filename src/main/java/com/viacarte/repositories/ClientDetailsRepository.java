package com.viacarte.repositories;

import java.util.List;

import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.ClientDetails;
import com.viacarte.entity.User;

public interface ClientDetailsRepository
		extends CrudRepository<ClientDetails, Long>, JpaSpecificationExecutor<ClientDetails> {

    ClientDetails findByClientHashId(String clientHashId);

    ClientDetails findByEntityName(String entityName);

    List<ClientDetails> findAll();
}
