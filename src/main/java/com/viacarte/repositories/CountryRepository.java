package com.viacarte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.Country;

public interface CountryRepository extends CrudRepository<Country, Long>, JpaSpecificationExecutor<Country> {



}
