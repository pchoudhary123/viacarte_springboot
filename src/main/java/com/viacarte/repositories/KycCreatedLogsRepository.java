package com.viacarte.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.viacarte.entity.KYCCreatedLogs;

public interface KycCreatedLogsRepository extends CrudRepository<KYCCreatedLogs, Long>, PagingAndSortingRepository<KYCCreatedLogs, Long>,
JpaSpecificationExecutor<KYCCreatedLogs> {



}
