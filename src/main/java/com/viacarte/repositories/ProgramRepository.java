package com.viacarte.repositories;

import com.viacarte.entity.Program;
import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProgramRepository extends CrudRepository<Program, Long>, JpaSpecificationExecutor<Program> {

    Program findByProgramName(String programName);

    List<Program> findAllByStatus(Status active);

    List<Program> findByIdIn(List<Long> programIds);
}
