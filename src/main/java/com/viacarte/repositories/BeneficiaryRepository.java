package com.viacarte.repositories;

import java.util.List;

import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.viacarte.entity.Beneficiary;
import com.viacarte.entity.User;

public interface BeneficiaryRepository extends CrudRepository<Beneficiary,Long>,PagingAndSortingRepository<Beneficiary, Long>, 
JpaSpecificationExecutor<Beneficiary>
{

}