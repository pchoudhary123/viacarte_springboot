package com.viacarte.repositories;

import java.util.Date;
import java.util.List;

import com.viacarte.enums.Status;
import com.viacarte.enums.UserType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.viacarte.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, PagingAndSortingRepository<User, Long>,
		JpaSpecificationExecutor<User>, UserRepositoryCustom {

	User findByUsername(String username);

	List<User> findByEmail(String primaryEmail);

	List<User> findByContactNo(String primaryContact);
}
