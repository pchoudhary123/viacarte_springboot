package com.viacarte.repositories;

import java.util.List;

import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.viacarte.entity.Issuer;

public interface IssuerRepository extends CrudRepository<Issuer,Long>,JpaSpecificationExecutor<Issuer>,PagingAndSortingRepository<Issuer, Long>
{
    @Query("SELECT p FROM Issuer p WHERE p.bankName=?1")
    Issuer getIssuerByName(String bankName);

    Issuer findById(long id);

    List<Issuer> findAllByStatus(Status active);
}
