package com.viacarte.repositories;

import java.util.List;

import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.InviteCustomer;

public interface InviteCustomerRepository
		extends JpaSpecificationExecutor<InviteCustomer>, CrudRepository<InviteCustomer, Long> {



}
