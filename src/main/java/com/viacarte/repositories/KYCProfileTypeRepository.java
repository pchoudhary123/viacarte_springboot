package com.viacarte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.KYCProfileType;

public interface KYCProfileTypeRepository
		extends CrudRepository<KYCProfileType, Long>, JpaSpecificationExecutor<KYCProfileType> {

	@Query("select kyc from KYCProfileType kyc where kyc.profileTypeName =?1")
	KYCProfileType getByName(String profileTypeName);

	@Query("select kyc from KYCProfileType kyc ")
	List<KYCProfileType> getKycProfileTypeList();

	List<KYCProfileType> findAll();
}
