package com.viacarte.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.viacarte.entity.User;
import com.viacarte.entity.UserSession;

public interface UserSessionRepository extends CrudRepository<UserSession, String> {

    @Query("SELECT us FROM UserSession us WHERE us.sessionId = ?1 AND us.expired = false")
    UserSession findByActiveSessionId(String sessionId);

    @Transactional
    @Modifying
    @Query("UPDATE UserSession us SET us.lastRequest = now() WHERE us.sessionId=?1")
    void refreshSession(String sessionId);

    @Query("SELECT us FROM UserSession us WHERE us.sessionId = ?1")
    UserSession findBySessionId(String sessionId);
}
