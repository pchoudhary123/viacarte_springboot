package com.viacarte.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.viacarte.entity.AccountDetails;
import com.viacarte.entity.AccountType;

public interface AccountDetailRepository extends CrudRepository<AccountDetails, Long>,
		PagingAndSortingRepository<AccountDetails, Long>, JpaSpecificationExecutor<AccountDetails> {



}