package com.viacarte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.AccountType;

public interface AccountTypeRepository
		extends CrudRepository<AccountType, Long>, JpaSpecificationExecutor<AccountType> {


	AccountType findByCode(String code);
}