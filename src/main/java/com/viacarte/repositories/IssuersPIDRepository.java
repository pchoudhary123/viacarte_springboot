package com.viacarte.repositories;

import java.util.List;

import com.viacarte.entity.Issuer;
import com.viacarte.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.viacarte.entity.IssuersPID;

public interface IssuersPIDRepository extends JpaSpecificationExecutor<IssuersPID>, CrudRepository<IssuersPID, Long> {

	IssuersPID findAllByPid(String pid);

	List<IssuersPID> findAllByStatus(Status active);

	@Query("select i from IssuersPID i where i.issuerHashId=?1 ")
	List<IssuersPID> getIssuerPIDByHashId(String issuerHashId);

}
