package com.viacarte.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.viacarte.enums.Status;
import com.viacarte.enums.UserType;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class CommonUtil {

    /**
     * Random Number Generator for MObile OTP return 6 digit random number
     *
     * @return
     */
    public static final long BASE_ACCOUNT_NUMBER = 999900910000000L;
    private static final double RADIUS_OF_EARTH = 6371.0D;
    public static String ITUNES_LINK = "https://goo.gl/Ou5ZRr";
    public static String PLAY_STORE_LINK = "http://goo.gl/07Kf4u";
    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat dateFormate2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static SimpleDateFormat formatter3 = new SimpleDateFormat("ddMMyyyy");
    public static SimpleDateFormat formatter4 = new SimpleDateFormat("MMM dd, yyyy");
    public static SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat formatter5 = new SimpleDateFormat("dd-MMM-yy");
    public static SimpleDateFormat formatter6 = new SimpleDateFormat("MMyy");
    public static String number = "^\\d+$";

    public static int proxyLength = 12;
    public static final String startTime = " 00:00:00";
    public static final String endTime = " 23:59:59";
    public static final String RandomString = "ARNABLAYEKarnablayek01234567890";
    public static final String approve = "approved";
    public static final String reject = "rejected";
    public final static String ERROR_MSG = "ERRORMSG";
    public final static String SUCCESS_MSG = "SUCCESSMSG";
    public final static int maxAuthAttempts = 5;
    public static final String urlLink = "http://13.234.125.216/Master/Home";
    static final String SOURCE = "0123456789ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdfghijklmnopqrstuvwxyz";
    static SecureRandom secureRnd = new SecureRandom();

    public static final String ipaisa_key_path = "/document";
    public static int maxNoOfVCards = 40;
    public static final String AWS_SECRET_ACCESS_KEY = "Im69nuAfmdTksh4KBj6gaMO83jxTsuhJ6q+xMr7U";
    public static final String AWS_ACCESS_KEY_ID = "AKIA5KFIM6QAOJ235NMD";
    public static final String CSV_BUCKET_NAME = "viacartetest";
    public static final String clientLogo_dir = "clientlogo/";
    public static final String cardImage_dir = "cardimage/";
    public static final String termsCondition_dir = "client_termscondition/";
    public static final String profile_image = "profile_image/";

    public static final String PINCODE_URL = "http://postalpincode.in/api/pincode/";

    public static final AWSCredentials credentials = new BasicAWSCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY);
    public static final AmazonS3 client = new AmazonS3Client(credentials);
    public static final String gpr_key_path = "client_dist/document";

    private static DecimalFormat decimalFormatter = new DecimalFormat("###.##");
    private static Pattern passPattern = Pattern
            .compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");

    public static final String user = "USER";
    public static final String dist = "DIST";
    public static final String agent = "AGEN";
    public static final String CLNT = "CLNT";
    public static final String PRGM = "PRGM";
    public static final String WAL = "WAL";
    public static final String CA = "CA";

    public static final String NO_IMAGE = "https://bit.ly/2VSpL6G";

    public static String generateSixDigitNumericString() {
        int number = (int) (Math.random() * 1000000);
        String formattedNumber = String.format("%06d", number);
        System.err.println(formattedNumber);
        return formattedNumber;
    }

    /**
     * random n digit number generator
     *
     * @param n
     * @return
     */

    public static String generateNDigitNumericString(long n) {
        double mul = Math.pow(10, n);
        long result = (long) (Math.random() * mul);
        // long result = 12;
        String number = String.format("%0" + n + "d", result);
        // System.err.println(number);
        return number;
    }

    public static String generateNineDigitNumericString() {
        return "" + (int) (Math.random() * 1000000000);
    }

    public static void sleep(long timeInMS) {
        try {
            Thread.sleep(timeInMS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static double calculateDistance(double lat1, double long1, double lat2, double long2) {
        double distance = 0.0D;
        double lat1InRad = Math.toRadians(lat1);
        double lat2InRad = Math.toRadians(lat2);
        double diffInLat = Math.toRadians(lat2 - lat1);
        double diffInLong = Math.toRadians(long2 - long1);
        double a = Math.pow(Math.sin(diffInLat / 2.0D), 2.0D)
                + Math.cos(lat1InRad) * Math.cos(lat2InRad) * Math.pow(Math.sin(diffInLong / 2.0D), 2.0D);
        distance = RADIUS_OF_EARTH * Math.atan2(Math.sqrt(a), Math.sqrt(1.0D - a));
        return distance;
    }

    public static String[] getDefaultDateRange(SimpleDateFormat dateFormate, int count) {
        String range[] = new String[2];
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, count);
        Date today30 = cal.getTime();
        range[0] = dateFormate.format(today30);
        range[1] = dateFormate.format(today);
        System.out.println(range[0] + ":" + range[1]);
        return range;
    }

    // local

    public static String[] getDateRange(String daterange) {
        String range[] = null;
        if (daterange != null) {
            System.err.println(daterange);
            try {
                range = daterange.split("-");
                range[0] = formatter.format(new Date(Long.parseLong(range[0].trim())));
                range[1] = formatter.format(new Date(Long.parseLong(range[1].trim())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return range;
    }

    public static String generateRandomString(String randomString) {
        if (randomString != null && !randomString.isEmpty()) {
            Random rand = new Random();
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < 6; i++) {
                int randIndex = rand.nextInt(randomString.length());
                res.append(randomString.charAt(randIndex));
            }
            return res.toString();
        }
        return null;
    }

    public static void main(String[] args) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Jon", "Ravi");
        hashMap.put("Arnab", "Layek");
        hashMap.put("qqq", "rrrr");
        hashMap.put("Ravi", "Jon");
        for (Map.Entry<String, String> str : hashMap.entrySet()) {
            if (str.getKey().equalsIgnoreCase(str.getValue())) {
                System.err.println("111");
            } else {
                System.err.println("222");
            }
        }

    }

  /*  public static double commissionEarned(Commission comm, double amount) {
        double finalComm = 0.0;
        if (comm.isFixed() == true) {
            finalComm = comm.getValue();
        } else {
            finalComm = (amount * comm.getValue()) / 100;
        }
        BigDecimal a = new BigDecimal(String.valueOf(finalComm));
        BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        return roundOff.doubleValue();
    }*/

    // FOR EMAIL CRON TO GET CARD REPORT SUMMARY
    public static Date getPreviousStartDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date date1 = cal.getTime();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

        String d = formatter2.format(date1);
        d = d + " 00:00:00";

        try {
            date1 = formatter1.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date1;
    }

    public static Date getPreviousToDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date date1 = cal.getTime();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

        String d = formatter2.format(date1);
        d = d + " 23:59:59";

        try {
            date1 = formatter1.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date1;

    }

    public static Date getStartMonthDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.MONTH, -1);
        aCalendar.set(Calendar.DATE, 1);
        Calendar cal = setTimeToBeginningOfDay(aCalendar);
        Date firstDateOfPreviousMonth = cal.getTime();
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return firstDateOfPreviousMonth;

    }

    private static Calendar setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static Date getMonthToDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.MONTH, -1);
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Calendar cal1 = setTimeToEndofDay(aCalendar);
        Date lastDateOfPreviousMonth = cal1.getTime();
        return lastDateOfPreviousMonth;

    }

    private static Calendar setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar;
    }

    /**
     * UPLOAD FCM
     */

    public static double roundTwoDigitAfterDecimal(double value) {
        double number = 0.0;
        if (value > 0.0) {
            DecimalFormat df = new DecimalFormat("#.##");
            number = Double.valueOf(df.format(value));
        }
        return number;
    }

    public static SOAPMessage createSOAPMessage() {
        SOAPMessage soapMessage = null;
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            soapMessage = messageFactory.createMessage();

        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return soapMessage;
    }

    public static SOAPConnection getNewConnection() {
        SOAPConnection soapConnection = null;
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return soapConnection;
    }

    public static String randomString(int length, String preVal) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(SOURCE.charAt(secureRnd.nextInt(SOURCE.length())));
        return preVal + sb.toString().toUpperCase();
    }

    public static String randomStringInvitationCode(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(SOURCE.charAt(secureRnd.nextInt(SOURCE.length())));
        return sb.toString().toUpperCase();
    }


    public static String getUserTypeList(String type) {
        StringBuffer buffer = new StringBuffer();
        if ("All".equalsIgnoreCase(type)) {
            buffer.append(UserType.valueOf(UserType.Agent.toString()).ordinal() + ",");
            buffer.append(UserType.valueOf(UserType.Distributor.toString()).ordinal());
        } else if ("Distributor".equalsIgnoreCase(type)) {
            buffer.append(UserType.valueOf(UserType.Distributor.toString()).ordinal());
        } else if ("Agent".equalsIgnoreCase(type)) {
            buffer.append(UserType.valueOf(UserType.Agent.toString()).ordinal());
        } else {
            buffer.append(UserType.valueOf(UserType.User.toString()).ordinal());
        }
        return buffer.toString();
    }

    public static String uploadFile(MultipartFile file, String clientHashId, String fileType) {
        String fileKey = clientHashId + "_" + System.nanoTime();
        client.putObject(new PutObjectRequest(CommonUtil.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
                .withCannedAcl(CannedAccessControlList.PublicRead));
        return String.format("https://%s.s3.amazonaws.com/%s", CommonUtil.CSV_BUCKET_NAME, fileKey);
    }

    public static String CsvUploadFile(MultipartFile file, String contact, String fileType) {
        String fileName = "";
        try {
            fileName = generateFileKey(file, contact, fileType);
            String fileKey = gpr_key_path + fileName;
            client.putObject(CSV_BUCKET_NAME, fileKey, convertFromMultipart(file));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    public static String generateFileKey(MultipartFile file, String contact, String image) {
        try {
            String systemMillis = String.valueOf(System.currentTimeMillis());
            return contact + "_" + image + systemMillis;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static File convertFromMultipart(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try {
            file.transferTo(convertedFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convertedFile;
    }

    public static String getStringFromObject(Object obj) {
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    public static String getDecimalValue(double value) {
        return decimalFormatter.format(value);
    }

    public static double getDoubleTypeValue(double value) {
        return Double.valueOf(getDecimalValue(value)).doubleValue();
    }

    public static String uploadImage(MultipartFile file, String contact, String idType, String directory) {
        String fileName = "";
        try {
            fileName = generateFileKey(file, idType, contact);
            String fileKey =directory+ fileName;
            InputStream is=file.getInputStream();
            client.putObject(new PutObjectRequest(CommonUtil.CSV_BUCKET_NAME, fileKey,is,new ObjectMetadata()).withCannedAcl(CannedAccessControlList.PublicRead));
            //client.putObject(new PutObjectRequest(CommonUtil.CSV_BUCKET_NAME, fileKey, convertFromMultipart(file))
                    //.withCannedAcl(CannedAccessControlList.PublicRead));
            return String.format("https://%s.s3.amazonaws.com/%s", CommonUtil.CSV_BUCKET_NAME, fileKey);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

 /*   public static String[] getClientKeyToken(String auth) {
        if (auth != null && !auth.isEmpty()) {
            try {
                String base64Credentials = auth.substring("Basic".length()).trim();
                String credentials = new String(Base64.decode(base64Credentials), Charset.forName("UTF-8"));
                return credentials.split(":", 2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }*/

    public static String getRefValue(int length) {
        String mesgRefNo = String.valueOf(System.nanoTime()) + "" + String.valueOf(System.currentTimeMillis());
        return mesgRefNo.substring(mesgRefNo.length() - length, mesgRefNo.length());
    }

    public static String getIdType(String documentIdType) {
        switch (documentIdType) {
            case "passport":
                return "1";
            case "driver_id":
                return "2";
            case "voter_id":
                return "3";
            case "social_security_card":
                return "4";
            case "medical_benefits_card":
                return "5";
            case "national_id_number":
                return "10";
        }
        return null;
    }

    public static String translate(String locale, String token) {
        try {
            String[] localeVal = getToLocale(locale);
            ResourceBundle bundle = ResourceBundle.getBundle("viacarte", Locale.US);
            Locale.setDefault(new Locale(localeVal[0], localeVal[1]));
            bundle = ResourceBundle.getBundle("viacarte");
            System.out.println("Message in " + Locale.getDefault() + ":" + bundle.getString(token));
            return bundle.getString(token);
        } catch (Exception e) {
            return "language conversion issue";
        }
    }

    private static String[] getToLocale(String locale) {
        String[] localeArr = new String[2];
        if ("en".equals(locale)) {
            localeArr[0] = locale;
            localeArr[1] = "US";
        }
        if ("es".equals(locale)) {
            localeArr[0] = locale;
            localeArr[1] = "ES";
        }
        if ("zh".equals(locale)) {
            localeArr[0] = locale;
            localeArr[1] = "ZH";
        }
        return localeArr;
    }

    public static String getJspName(String locale, String jspName) {
        if ("en".equals(locale)) {
            return "imoney/admin/" + jspName;
        }
        if ("es".equals(locale)) {
            return "imoney/admin/spanish/" + jspName;
        }
        if ("zh".equals(locale)) {
            return "imoney/admin/chinese/" + jspName;
        }
        return "imoney/admin/" + jspName;
    }

    public static String getMasterJspName(String locale, String jspName) {
        if ("en".equals(locale)) {
            return "imoney/master/" + jspName;
        }
        if ("es".equals(locale)) {
            return "imoney/master/spanish/" + jspName;
        }
        if ("zh".equals(locale)) {
            return "imoney/master/chinese/" + jspName;
        }
        return "imoney/master/" + jspName;
    }

    public static Status getStatus(String action) {
        switch (action) {
            case "activate_card":
                return Status.Active;
            case "close_card":
                return Status.Locked;
            case "block_only":
                return Status.Blocked;
        }
        return null;
    }

    public static Properties getEmailProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "*");
        return props;
    }

    public static String validPassword(String pass) {
        if (pass == null || pass.length() < 8) {
            return "Please enter minimum 8 character length password";
        } else {
            if (!passPattern.matcher(pass).matches()) {
                return "Password must be alphanumeric combination of lowercase, uppercase and special characters";
            }
        }
        return null;
    }
    public static String convertToEmptyIfNull(String input) {
        if (input == null) {
            return "";
        } else {
            return input;
        }
    }
    public static List<Long> commaSeparatedStringToList(String string) {
        // NOTE : This function will only convert Integer values
        List<Long> list = new ArrayList<Long>();
        if (string != null && !string.isEmpty()) {
            List<String> intAsString = Arrays.asList(string.split(","));
            for (String items : intAsString) {
                list.add(Long.parseLong(items));
            }
        }
        return list;
    }

}
