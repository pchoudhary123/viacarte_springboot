package com.viacarte.util;

public interface Authorities {

    public static final String AUTHENTICATED = "ROLE_AUTHENTICATED";

    public static final String BLOCKED = "ROLE_BLOCKED";

    public static final String LOCKED = "ROLE_LOCKED";

    public static final String ADMINISTRATOR = "ROLE_ADMINISTRATOR";

    public static final String SUPER_ADMINISTRATOR = "ROLE_SUPER_ADMINISTRATOR";

    public static final String USER = "ROLE_USER";

    public static final String SUPER_DISTRIBUTOR = "ROLE_SUPER_DISTRIBUTOR";

    public static final String DISTRIBUTOR = "ROLE_DISTRIBUTOR";

    public static final String SUPPORT = "ROLE_SUPPORT";

    public static final String COMPLAINT = "ROLE_COMPLAINT";

    public static final String ACCOUNTS = "ROLE_ACCOUNTS";

    public static final String KYC_APPROVER = "ROLE_KYC_APPROVER";
    public static final String CLIENT= "ROLE_CLIENT";

}