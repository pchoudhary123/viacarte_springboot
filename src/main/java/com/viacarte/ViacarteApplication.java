package com.viacarte;

import com.viacarte.util.CustomBcryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableJpaRepositories
@EnableCaching
@EnableScheduling
public class ViacarteApplication extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    @Autowired
    @Qualifier(value = "LoginUserDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    public static void main(String[] args) {
        SpringApplication.run(ViacarteApplication.class, args);
    }


    @Bean
    public AuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder());
        return daoAuthenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/auth/logout").permitAll().antMatchers("/static/**").permitAll().and()
                .authorizeRequests().antMatchers("/auth/resetPassword").permitAll().and().authorizeRequests()
                .antMatchers("/product/resetPassword").permitAll().antMatchers("/**/auth/program").permitAll().and()
                .authorizeRequests().antMatchers("/actuator/**").permitAll();
        http.authorizeRequests().anyRequest().authenticated().and().formLogin()
                .loginPage("/Master_Admin/login").permitAll()
                .loginProcessingUrl("/Master_Admin/loginPost")
                .usernameParameter("loginUsername")
                .passwordParameter("loginPassword").defaultSuccessUrl("/Master_Admin/index")
                .permitAll().failureUrl("/Master_Admin/login?error=true").and().sessionManagement().maximumSessions(1)
                .expiredUrl("/Master_Admin/login?expired=true").maxSessionsPreventsLogin(false)

                .and().and().csrf().disable()

                .exceptionHandling().accessDeniedPage("/Master_Admin/error")

                .and().cors();

//				.and().addFilterAfter(customFilter, ChannelProcessingFilter.class);
    }
}