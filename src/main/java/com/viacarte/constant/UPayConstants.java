package com.viacarte.constant;

import java.nio.charset.StandardCharsets;

import javax.xml.bind.DatatypeConverter;

public class UPayConstants {

	// public static final boolean production =true;
	public static final boolean production = false;
	private static final String viacarte_uat = "http://34.216.94.123/viacarte_card"; // live
	//private static final String viacarte_uat = "/home/msewa-user/Viacarte"; // live
	private static final String viacarte_live = "";

	private static final String viacarte_user_uat = "http://localhost:9093/imoney";
	private static final String viacarte_user_live = "";

	private static final String base_url_uat = "https://gpcantigua-uat.konycloud.com/services/issuing/BKVH7YGFJ69CL3MDUW/programs/";
	private static final String base_url_live = "https://gpcantigua.konycloud.com/services/issuing/BKVH7YGFJ69CL3MDUW/programs/";
	

	private static final String basic_auth_username_uat = "75278670021733c09ce879a570017db4"; // key
	private static final String basic_auth_password_uat = "be6f914a1e7b0fbae49200aeb1c8f9d8"; // token

	private static final String basic_auth_username_live = "db7e001c0bae37950737556836c0cac1"; // key
	private static final String basic_auth_password_live = "4c893cc9f5bb8e113463c4fd7295a37a"; // token

	
//	==================Login Details================

	public static final String login_base_url = "https://100021101.auth.konycloud.com/login?provider=global";
	
	public static final String login_username= "pidQDC0SZAVA4JKQNESYHOEWFI2BJAQ96TX0GG8";
	public static final String login_password= "s|uA`K|2iK26^=T*8s@llCW.\"";

	private static final String login_username_live = "";
	private static final String login_password_live = "";

	// PID and MVA is common for each

	private static final String PID_UAT = "QDC0SZAVA4JKQNESYHOEWFI2BJAQ96TX0GG8";
	private static final String MVA_UAT = "GBCREF02732842";

	public static String currency_code = "840";

	public static final String assign_card = "/cards/";
	public static final String accounts = "/accounts";
	public static final String load_card = "/transactions/loads";
	public static final String transactions = "/transactions";
	public static final String p2p = "/p2p";
	public static final String pin = "/pin";

	public static String getBasicUrl() {
		return production ? base_url_live : base_url_uat;
	}

	public static String getPID() {
		return production ? PID_UAT : PID_UAT;
	}

	public static String getMVA() {
		return production ? MVA_UAT : MVA_UAT;
	}

	public static String getKey() {
		return production ? basic_auth_username_live : basic_auth_username_uat;
	}

	public static String getToken() {
		return production ? basic_auth_password_live : basic_auth_password_uat;
	}

	public static String getUrl() {
		return production ? viacarte_live : viacarte_uat;
	}

	public static String getUserInterfaceUrl() {
		return production ? viacarte_user_live : viacarte_user_uat;
	}


	public static String getBasicAuthorization() {
		String result = "Basic ";
		String credentials = getKey() + ":" + getToken();
		result = result + DatatypeConverter.printBase64Binary(credentials.getBytes(StandardCharsets.UTF_8));
		return result;
	}

}
