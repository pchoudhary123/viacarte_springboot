package com.viacarte.request;

import java.util.List;

import com.viacarte.enums.UserType;
import org.springframework.web.multipart.MultipartFile;



public class RequestJson  {


    private String addressLine1;
    private String addressLine2;
    private String countryCode;
    private String username;
    private String deviceId;
    private String device;
    private UserType userType;
    private String otp;
    private String gcmId;
    private String pin;
    private String mobileNumber;
    private String menuItem;
    private String contactNo;
    private String androidDeviceID;
    private String role;
    private boolean updateAdmin;
    private List<String> menuItems;
    private String key;
    private String password;
    private String ipAddress;
    private String registrationId;
    private String newMpin;
    private String confirmMpin;
    private String oldMpin;
    private MultipartFile addressproof;
    private MultipartFile idproof;
    private MultipartFile file;
    private String daterange;
    private String component;
    private String comment;
    private String ticketType;
    private String ticketNo;
    private String status;
    private String customerId;
    private String serviceTypeId;
    private double amount;
    private String confirmPassword;
    private String oldPassword;
    private String newPassword;
    private String loadMoneyRequest;
    private String transactionRefNo;
    private String pgRefId;
    private String rejectType;
    private String rejectReason;
    private String action;
    private String fileId;
    private String addressType;
    private String addressIdNum;
    private String clientHashId;
    // Topup and Bill pay
    private String topUpType;
    private String serviceProvider;
    private String billingUnit;
    private String accountNumber;
    private String rechargeType;
    private String serviceName;
    private String rechargeNumber;
    private String circle;
    private String dthNo;
    private String landlineNumber;
    private String additionalInfo;
    private String stdCode;
    private String referenceNumber;
    private String processingCycle;
    private String cityName;
    private String cycleNumber;
    private String subDivisionCode;
    private String billGroupNumber;
    private String emailId;
    private String kNumber;
    private String policyNumber;
    private String policyDate;
    private String senderMobileNo;
    private String documentIdNo;
    private String documentIdType;
    private String oldPin;
    private String address2;
    private String addressDocLanguage;
    private String addressIssuedBy;
    /* private String addressProofFilePath; */
    private String addressPath;
    private String idPath;
    private MultipartFile addressProofFilePath;
    private String addressProofFilePathSrc;
    private String addressProofType;
    private String issueDateAddressProof;
    private String idProofCountry;
    private MultipartFile idProofFilePath;
    private String idProofFilePathSrc;
    private String idProofNumber;
    private String idProofType;
    private String issueDateIdProof;
    private String expiryDateIdProof;
    private String verificationCode;
    private String languageId;
    private String selectCountry;
    private String expiryDate1;
    private String issueDate1;
    private String addressProof;
    private String issueBy;
    private String issueDate2;
    private String docLanguage;
    private String file1;
    private String confPin;
    private String name;
    private String userImage;
    private String primaryName;
    private String billingName;
    private String billingPhone;
    private String billingEmail;
    private String beneficiaryStatus;
    private String entityname;
    private String bankName;
    private String brand;
    private String programHashId;
    private MultipartFile idProofFilePathBack;
    private MultipartFile addressProofFilePathBack;
    private String idProofFilePathBackSrc;
    private String addressProofFilePathBackSrc;
    private String cardId;
    private String email_radio;
    private String colorCode;
    private String nickName;
    private String clientUrlId;
    private String rejectionReason;
    private String productCode;
    private String adminEmail;
    private String adminName;
    private String clientDetailsStatus;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String gender;
    private String postalCode;
    private String city;
    private String state;
    private String address1;
    private String idType;
    private String idNo;
    private String language;
    private String Bill;
    private String s3path;
    private String kitNumber;
    private String userHashId;
    private String otpSendBy;
    private String invitationCode;
    private String viewName;
    private long question1;
    private long question2;
    private long question3;
    private String answer1;
    private String answer2;
    private String answer3;
    private long progressBar;
    private String receiver;
    private String cardHashId;
    private String cardPassword;
    private String modules;
    private String receiverCardHashId;
    private String walletHashId;
    private String communication;
    private String twitterAccount;
    private String facebookAccount;
    private String languageUser;
    private String timeZone;
    private Long idProofId;
    private String suffix;
    private String title;


    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getIdProofId() {
        return idProofId;
    }

    public void setIdProofId(Long idProofId) {
        this.idProofId = idProofId;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public String getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(String facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public String getLanguageUser() {
        return languageUser;
    }

    public void setLanguageUser(String languageUser) {
        this.languageUser = languageUser;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getWalletHashId() {
        return walletHashId;
    }

    public void setWalletHashId(String walletHashId) {
        this.walletHashId = walletHashId;
    }

    public String getReceiverCardHashId() {
        return receiverCardHashId;
    }

    public void setReceiverCardHashId(String receiverCardHashId) {
        this.receiverCardHashId = receiverCardHashId;
    }

    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    public String getCardPassword() {
        return cardPassword;
    }

    public void setCardPassword(String cardPassword) {
        this.cardPassword = cardPassword;
    }

    public String getCardHashId() {
        return cardHashId;
    }

    public void setCardHashId(String cardHashId) {
        this.cardHashId = cardHashId;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public long getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(long progressBar) {
        this.progressBar = progressBar;
    }

    public long getQuestion3() {
        return question3;
    }

    public void setQuestion3(long question3) {
        this.question3 = question3;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public long getQuestion2() {
        return question2;
    }

    public void setQuestion2(long question2) {
        this.question2 = question2;
    }

    public long getQuestion1() {
        return question1;
    }

    public void setQuestion1(long question1) {
        this.question1 = question1;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getOtpSendBy() {
        return otpSendBy;
    }

    public void setOtpSendBy(String otpSendBy) {
        this.otpSendBy = otpSendBy;
    }

    public String getUserHashId() {
        return userHashId;
    }

    public void setUserHashId(String userHashId) {
        this.userHashId = userHashId;
    }

    public String getKitNumber() {
        return kitNumber;
    }

    public void setKitNumber(String kitNumber) {
        this.kitNumber = kitNumber;
    }

    public String getS3path() {
        return s3path;
    }

    public void setS3path(String s3path) {
        this.s3path = s3path;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getBill() {
        return Bill;
    }

    public void setBill(String bill) {
        Bill = bill;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getClientUrlId() {
        return clientUrlId;
    }

    public void setClientUrlId(String clientUrlId) {
        this.clientUrlId = clientUrlId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getAddressProofFilePathSrc() {
        return addressProofFilePathSrc;
    }

    public void setAddressProofFilePathSrc(String addressProofFilePathSrc) {
        this.addressProofFilePathSrc = addressProofFilePathSrc;
    }

    public String getIdProofFilePathSrc() {
        return idProofFilePathSrc;
    }

    public void setIdProofFilePathSrc(String idProofFilePathSrc) {
        this.idProofFilePathSrc = idProofFilePathSrc;
    }

    public String getIdProofFilePathBackSrc() {
        return idProofFilePathBackSrc;
    }

    public void setIdProofFilePathBackSrc(String idProofFilePathBackSrc) {
        this.idProofFilePathBackSrc = idProofFilePathBackSrc;
    }

    public String getAddressProofFilePathBackSrc() {
        return addressProofFilePathBackSrc;
    }

    public void setAddressProofFilePathBackSrc(String addressProofFilePathBackSrc) {
        this.addressProofFilePathBackSrc = addressProofFilePathBackSrc;
    }

    public String getEmail_radio() {
        return email_radio;
    }

    public void setEmail_radio(String email_radio) {
        this.email_radio = email_radio;
    }

    public String getBeneficiaryStatus() {
        return beneficiaryStatus;
    }

    public void setBeneficiaryStatus(String beneficiaryStatus) {
        this.beneficiaryStatus = beneficiaryStatus;
    }

    public String getClientDetailsStatus() {
        return clientDetailsStatus;
    }

    public void setClientDetailsStatus(String clientDetailsStatus) {
        this.clientDetailsStatus = clientDetailsStatus;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public MultipartFile getIdProofFilePathBack() {
        return idProofFilePathBack;
    }

    public void setIdProofFilePathBack(MultipartFile idProofFilePathBack) {
        this.idProofFilePathBack = idProofFilePathBack;
    }

    public MultipartFile getAddressProofFilePathBack() {
        return addressProofFilePathBack;
    }

    public void setAddressProofFilePathBack(MultipartFile addressProofFilePathBack) {
        this.addressProofFilePathBack = addressProofFilePathBack;
    }

    public String getProgramHashId() {
        return programHashId;
    }

    public void setProgramHashId(String programHashId) {
        this.programHashId = programHashId;
    }

    public String getClientHashId() {
        return clientHashId;
    }

    public void setClientHashId(String clientHashId) {
        this.clientHashId = clientHashId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getEntityname() {
        return entityname;
    }

    public void setEntityname(String entityname) {
        this.entityname = entityname;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getAddressPath() {
        return addressPath;
    }

    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConfPin() {
        return confPin;
    }

    public void setConfPin(String confPin) {
        this.confPin = confPin;
    }

    public String getSelectCountry() {
        return selectCountry;
    }

    public void setSelectCountry(String selectCountry) {
        this.selectCountry = selectCountry;
    }

    public String getExpiryDate1() {
        return expiryDate1;
    }

    public void setExpiryDate1(String expiryDate1) {
        this.expiryDate1 = expiryDate1;
    }

    public String getIssueDate1() {
        return issueDate1;
    }

    public void setIssueDate1(String issueDate1) {
        this.issueDate1 = issueDate1;
    }

    public String getAddressProof() {
        return addressProof;
    }

    public void setAddressProof(String addressProof) {
        this.addressProof = addressProof;
    }

    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

    public String getIssueDate2() {
        return issueDate2;
    }

    public void setIssueDate2(String issueDate2) {
        this.issueDate2 = issueDate2;
    }

    public String getDocLanguage() {
        return docLanguage;
    }

    public void setDocLanguage(String docLanguage) {
        this.docLanguage = docLanguage;
    }

    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddressDocLanguage() {
        return addressDocLanguage;
    }

    public void setAddressDocLanguage(String addressDocLanguage) {
        this.addressDocLanguage = addressDocLanguage;
    }

    public String getAddressIssuedBy() {
        return addressIssuedBy;
    }

    public void setAddressIssuedBy(String addressIssuedBy) {
        this.addressIssuedBy = addressIssuedBy;
    }

    public String getAddressProofType() {
        return addressProofType;
    }

    public void setAddressProofType(String addressProofType) {
        this.addressProofType = addressProofType;
    }

    public String getIssueDateAddressProof() {
        return issueDateAddressProof;
    }

    public void setIssueDateAddressProof(String issueDateAddressProof) {
        this.issueDateAddressProof = issueDateAddressProof;
    }

    public String getIdProofCountry() {
        return idProofCountry;
    }

    public void setIdProofCountry(String idProofCountry) {
        this.idProofCountry = idProofCountry;
    }

    public MultipartFile getAddressProofFilePath() {
        return addressProofFilePath;
    }

    public void setAddressProofFilePath(MultipartFile addressProofFilePath) {
        this.addressProofFilePath = addressProofFilePath;
    }

    public MultipartFile getIdProofFilePath() {
        return idProofFilePath;
    }

    public void setIdProofFilePath(MultipartFile idProofFilePath) {
        this.idProofFilePath = idProofFilePath;
    }

    public String getIdProofNumber() {
        return idProofNumber;
    }

    public void setIdProofNumber(String idProofNumber) {
        this.idProofNumber = idProofNumber;
    }

    public String getIdProofType() {
        return idProofType;
    }

    public void setIdProofType(String idProofType) {
        this.idProofType = idProofType;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getIssueDateIdProof() {
        return issueDateIdProof;
    }

    public void setIssueDateIdProof(String issueDateIdProof) {
        this.issueDateIdProof = issueDateIdProof;
    }

    public String getExpiryDateIdProof() {
        return expiryDateIdProof;
    }

    public void setExpiryDateIdProof(String expiryDateIdProof) {
        this.expiryDateIdProof = expiryDateIdProof;
    }

    public String getOldPin() {
        return oldPin;
    }

    public void setOldPin(String oldPin) {
        this.oldPin = oldPin;
    }

    public String getDocumentIdNo() {
        return documentIdNo;
    }

    public void setDocumentIdNo(String documentIdNo) {
        this.documentIdNo = documentIdNo;
    }

    public String getDocumentIdType() {
        return documentIdType;
    }

    public void setDocumentIdType(String documentIdType) {
        this.documentIdType = documentIdType;
    }

    public String getSenderMobileNo() {
        return senderMobileNo;
    }

    public void setSenderMobileNo(String senderMobileNo) {
        this.senderMobileNo = senderMobileNo;
    }

    public String getDthNo() {
        return dthNo;
    }

    public void setDthNo(String dthNo) {
        this.dthNo = dthNo;
    }

    public String getLandlineNumber() {
        return landlineNumber;
    }

    public void setLandlineNumber(String landlineNumber) {
        this.landlineNumber = landlineNumber;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getProcessingCycle() {
        return processingCycle;
    }

    public void setProcessingCycle(String processingCycle) {
        this.processingCycle = processingCycle;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(String cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public String getSubDivisionCode() {
        return subDivisionCode;
    }

    public void setSubDivisionCode(String subDivisionCode) {
        this.subDivisionCode = subDivisionCode;
    }

    public String getBillGroupNumber() {
        return billGroupNumber;
    }

    public void setBillGroupNumber(String billGroupNumber) {
        this.billGroupNumber = billGroupNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getkNumber() {
        return kNumber;
    }

    public void setkNumber(String kNumber) {
        this.kNumber = kNumber;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyDate() {
        return policyDate;
    }

    public void setPolicyDate(String policyDate) {
        this.policyDate = policyDate;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(String rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(String billingUnit) {
        this.billingUnit = billingUnit;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTopUpType() {
        return topUpType;
    }

    public void setTopUpType(String topUpType) {
        this.topUpType = topUpType;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressIdNum() {
        return addressIdNum;
    }

    public void setAddressIdNum(String addressIdNum) {
        this.addressIdNum = addressIdNum;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getRejectType() {
        return rejectType;
    }

    public void setRejectType(String rejectType) {
        this.rejectType = rejectType;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }

    public String getPgRefId() {
        return pgRefId;
    }

    public void setPgRefId(String pgRefId) {
        this.pgRefId = pgRefId;
    }

    public String getLoadMoneyRequest() {
        return loadMoneyRequest;
    }

    public void setLoadMoneyRequest(String loadMoneyRequest) {
        this.loadMoneyRequest = loadMoneyRequest;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(String menuItem) {
        this.menuItem = menuItem;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAndroidDeviceID() {
        return androidDeviceID;
    }

    public void setAndroidDeviceID(String androidDeviceID) {
        this.androidDeviceID = androidDeviceID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(boolean updateAdmin) {
        this.updateAdmin = updateAdmin;
    }

    public List<String> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<String> menuItems) {
        this.menuItems = menuItems;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getNewMpin() {
        return newMpin;
    }

    public void setNewMpin(String newMpin) {
        this.newMpin = newMpin;
    }

    public String getConfirmMpin() {
        return confirmMpin;
    }

    public void setConfirmMpin(String confirmMpin) {
        this.confirmMpin = confirmMpin;
    }

    public String getOldMpin() {
        return oldMpin;
    }

    public void setOldMpin(String oldMpin) {
        this.oldMpin = oldMpin;
    }

    public MultipartFile getAddressproof() {
        return addressproof;
    }

    public void setAddressproof(MultipartFile addressproof) {
        this.addressproof = addressproof;
    }

    public MultipartFile getIdproof() {
        return idproof;
    }

    public void setIdproof(MultipartFile idproof) {
        this.idproof = idproof;
    }

    public String getDaterange() {
        return daterange;
    }

    public void setDaterange(String daterange) {
        this.daterange = daterange;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
