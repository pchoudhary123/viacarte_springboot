package com.viacarte.validation;


import com.viacarte.dto.*;
import com.viacarte.entity.KYCProfileType;
import com.viacarte.enums.BizErrors;
import com.viacarte.enums.Status;
import com.viacarte.exception.BadRequestException;
import com.viacarte.request.RequestJson;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Service
public class LoginValidation {


	public String checkLoginValidation(LoginDTO login) {
		if (CommonValidation.isNull(login.getUsername())) {
			return "Enter Username";
		} else if (CommonValidation.isNull(login.getPassword())) {
			return "Enter Password";
		}
		return null;
	}

	public String validateKYCProfileType(KYCProfileTypeDTO kyProfileType) {
		if (StringUtils.isBlank(kyProfileType.getCurrency()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "currency can not be empty");
		if (StringUtils.isBlank(kyProfileType.getPoaDefinition()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "proof of address(POA) definition  can not be empty");
		if (StringUtils.isBlank(kyProfileType.getPoiDefinition()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "proof of identification(POI) definition can not be empty");
		if (StringUtils.isBlank(kyProfileType.getProfileTypeName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ProfileTypeName can not be empty");
		if (kyProfileType.getPoaCount() ==0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "POA count can not be empty");
		if (kyProfileType.getPoiCount() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ProfileTypeName can not be empty");

		return null;
	}

	public String validateAddIssuer() {
		return null;
	}

	public String validateIssuer(IssuerDTO issuerDTO) {
		if (StringUtils.isBlank(issuerDTO.getBankName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BankName can not be empty");
		if (StringUtils.isBlank(issuerDTO.getBrand()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Brand name  can not be empty");
		if (StringUtils.isBlank(issuerDTO.getIdType()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "KycProfileType can not be empty");
		return null;
	}

	public String validateIssuersPID(IssuersPidDTO issuersPidDTO) {
		if (StringUtils.isBlank(issuersPidDTO.getIssuerHashId()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Issuer can not be empty");
		if (StringUtils.isBlank(issuersPidDTO.getPid()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), " Program ID can not be empty");
		if (StringUtils.isBlank(issuersPidDTO.getIssuerName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "IssuerName can not be empty");
		return null;
	}

	public String validateCardArray(CardArrayDTO cardArrayDTO) throws IOException {
		if (StringUtils.isBlank(cardArrayDTO.getCardName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "CardName can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getCurrency()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Currency can not be empty");
		if (cardArrayDTO.getIssuerId() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "CardIssuer can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getPid()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "program Type can not be empty");
		if (cardArrayDTO.getMaximumBalanceLimit1() ==0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "MaximumBalanceLimit1 can not be empty");
		if (cardArrayDTO.getMaximumBalanceLimit2() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "MaximumBalanceLimit2 can not be empty");
		if (cardArrayDTO.getLimits() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "limits can not be empty");
		if (cardArrayDTO.getBalanceLimit1() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BalanceLimit1 can not be empty");
		if (cardArrayDTO.getBalanceLimit2() == 0)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BalanceLimit2 can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getHeader1()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Header1 can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getHeader2()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Header2 Type can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getBenifitPoint1()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BenifitPoint1 can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getBenifitPoint2()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BenifitPoint2 can not be empty");
		if (StringUtils.isBlank(cardArrayDTO.getBenifitPoint3()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BenifitPoint3 Type can not be empty");
		if(cardArrayDTO.getCardImage().getSize() >0) {
			BufferedImage bimg = ImageIO.read(cardArrayDTO.getCardImage().getInputStream());
			int width          = bimg.getWidth();
			int height         = bimg.getHeight();
			if(width != 1000 || height != 635 ) {
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Height and Width must not exceed 1000W*635H for Card Image");
			}
		}
		return null;
	}

	public String validateProgram(ProgramDTO programDTO) {

		if (StringUtils.isBlank(programDTO.getProgramName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ProgramName can not be empty");
		if(programDTO.getTermsCondition().isEmpty())
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), " TermsCondition can not be empty");
		if (StringUtils.isBlank(programDTO.getCardModules()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "CardModules can not be empty");
		return null;
	}


	public String validateClientDetails(ClientDetailDTO clientDetailDTO) throws IOException {
		if (StringUtils.isBlank(clientDetailDTO.getEntityName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "EntityName can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getPrimaryContact()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "PrimaryContact can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getPrimaryName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "PrimaryName can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getPrimaryEmail()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "PrimaryEmail can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getBillingEmail()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BillingEmail can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getBillingName()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BillingName can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getBillingPhone()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "BillingPhone can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getAddress()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Address can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getColorCode()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ColorCode can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getProgramIds()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "ProgramIds can not be empty");
		if (StringUtils.isBlank(clientDetailDTO.getCountry()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Country can not be empty");
		if(clientDetailDTO.getClientLogo().getSize() >0) {
			BufferedImage bimg = ImageIO.read(clientDetailDTO.getClientLogo().getInputStream());
			int width          = bimg.getWidth();
			int height         = bimg.getHeight();
			if(width != 450 || height != 115) {
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Height and Width must not exceed 450W*115H for Client Logo");
			}
		}
		return null;
	}
}