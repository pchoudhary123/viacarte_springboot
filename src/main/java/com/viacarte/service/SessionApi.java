package com.viacarte.service;

import java.util.Date;
import java.util.List;

import com.viacarte.response.ResponseJson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.viacarte.entity.User;
import com.viacarte.entity.UserSession;
import com.viacarte.session.UserDetailsWrapper;
import com.viacarte.session.UserSessionDTO;

import javax.servlet.http.HttpServletRequest;

public interface SessionApi {


	UserSession getActiveUserSession(String sessionId);

	void refreshSession(String sessionId);

	ResponseJson registerNewSession(HttpServletRequest request);
}
