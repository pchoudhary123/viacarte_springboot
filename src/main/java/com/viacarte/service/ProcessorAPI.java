package com.viacarte.service;

import com.viacarte.dto.*;
import com.viacarte.response.ResponseJson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ProcessorAPI {


    ResponseJson createKycProfileType(KYCProfileTypeDTO kycProfileTypeDTO, HttpServletRequest request, HttpServletResponse response);

    ResponseJson listKycProfileTypes(HttpServletRequest request);


    Object addIssuer(HttpServletRequest request);

    ResponseJson addIssuer(IssuerDTO issuerDTO, HttpServletRequest request, HttpServletResponse response);

    Object getIssuers(HttpServletRequest request);

    ResponseJson createIssuersPID(IssuersPidDTO issuersPidDTO, HttpServletRequest request, HttpServletResponse response);

    Object getIssuersPID(HttpServletRequest request);

    Object getIssuersAndKycProfileTypes(HttpServletRequest request);

    ResponseJson addCardArray(CardArrayDTO cardArrayDTO, HttpServletRequest request, HttpServletResponse response) throws IOException;

    Object getCardArray(HttpServletRequest request);

    ResponseJson addProgram(ProgramDTO programDTO, HttpServletRequest request, HttpServletResponse response);

    Object getModules(HttpServletRequest request);

    Object getProgramList(HttpServletRequest request);

    ResponseJson addClientDetail(ClientDetailDTO clientDetailDTO, HttpServletRequest request, HttpServletResponse response) throws IOException;

    Object getClientList(HttpServletRequest request);
}
