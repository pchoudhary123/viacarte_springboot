
package com.viacarte.service.impl;

import com.viacarte.entity.User;
import com.viacarte.entity.UserSession;
import com.viacarte.service.UserApi;
import com.viacarte.util.Authorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service(value = "LoginUserDetailsServiceImpl")
public class LoginUserDetailsServiceImpl implements UserDetailsService {

	private final UserDetailsImpl userDetailsImpl;
	private final HttpServletRequest request;
	private final UserApi userApi;


	@Autowired
	public LoginUserDetailsServiceImpl(final UserDetailsImpl userDetailsImpl, final HttpServletRequest request, final UserApi userApi) {
		this.userDetailsImpl = userDetailsImpl;
		this.request = request;
		this.userApi = userApi;
	}
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		try {
			String password = request.getParameter("password");
			User user = userApi.findByUserName(username);
			if (user != null && (user.getAuthority().contains(Authorities.SUPER_DISTRIBUTOR))
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				if (user != null) {
					userDetailsImpl.setUserName(user.getUsername());
					userDetailsImpl.setPassword(user.getPassword());
					request.getSession().setAttribute("username",username);
					request.getSession().setAttribute("password",password);
				} else {
					throw new UsernameNotFoundException("user does not exist!");
				}
			} else {
				throw new UsernameNotFoundException("user does not exist!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDetailsImpl;
	}
}
