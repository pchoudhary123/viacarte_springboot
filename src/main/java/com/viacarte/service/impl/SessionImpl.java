package com.viacarte.service.impl;


import com.viacarte.entity.User;
import com.viacarte.entity.UserSession;
import com.viacarte.enums.BizErrors;
import com.viacarte.exception.BizException;
import com.viacarte.repositories.UserRepository;
import com.viacarte.repositories.UserSessionRepository;
import com.viacarte.response.ResponseJson;
import com.viacarte.service.UserApi;
import com.viacarte.util.CommonUtil;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;
import java.util.Date;


@Service
@Transactional
public class SessionImpl implements com.viacarte.service.SessionApi {

	private final UserSessionRepository userSessionRepository;
	private final UserRepository userRepository;
	private final UserApi userApi;

	public SessionImpl(UserSessionRepository userSessionRepository, UserRepository userRepository, UserApi userApi) {
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
		this.userApi = userApi;
	}

	@Override
	public UserSession getActiveUserSession(String sessionId) {
		return userSessionRepository.findByActiveSessionId(sessionId);
	}

	@Override
	public void refreshSession(String sessionId) {
		userSessionRepository.refreshSession(sessionId);
	}

	@Override
	public ResponseJson registerNewSession(HttpServletRequest request) {

		ResponseJson respJson = new ResponseJson();
		User user = userApi.findByUserName((String) request.getSession().getAttribute("username"));
		if (user == null)
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User not found!invalid username");
		try {
			UserSession userSession;
			userSession = userSessionRepository
					.findBySessionId(request.getSession().getId());
			if (userSession == null) {
				userSession = new UserSession();
				userSession.setSessionId(request.getSession().getId());
			}
			userSession.setUser(userRepository.findByUsername(user
					.getUsername()));
			userSession.setLastRequest(new Date());
			userSessionRepository.save(userSession);
			respJson.setDetails(userSession);
			respJson.setSuccess(true);
			return respJson;
		}catch (Exception e) {
			e.printStackTrace();
			throw new BizException(BizErrors.APPLICATION_ERROR.getValue(), "session wrong!");
		}
	}
}

