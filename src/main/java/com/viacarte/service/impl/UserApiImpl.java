package com.viacarte.service.impl;


import com.viacarte.dto.LoginDTO;
import com.viacarte.entity.ClientDetails;
import com.viacarte.entity.User;
import com.viacarte.enums.BizErrors;
import com.viacarte.exception.BadRequestException;
import com.viacarte.repositories.UserRepository;
import com.viacarte.response.ApiError;
import com.viacarte.response.ResponseJson;
import com.viacarte.service.UserApi;
import com.viacarte.util.Authorities;
import com.viacarte.util.CommonUtil;
import com.viacarte.validation.LoginValidation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


@Service
public class UserApiImpl implements UserApi {

	private final UserRepository userRepository;


@Autowired
	public UserApiImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
}


	@Override
	public User findByUserName(String name) {
		User user = userRepository.findByUsername(name);
		return user;
	}


	@Override
	public ResponseEntity<?> sendOTP(LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		if (StringUtils.isBlank(loginDTO.getUsername()) )
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user!");
		User user = userRepository.findByUsername(loginDTO.getUsername());
		if (user == null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User not found!invalid username");
		try {
			if (user != null && (user.getAuthority().contains(Authorities.SUPER_DISTRIBUTOR))
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					user.setMobileToken("123456");
					userRepository.save(user);
				/*if (loginDTO.getOtpSendBy().equalsIgnoreCase("email")) {
					respJson = userApi.sendOtpToClientByMail(user);
				} else {
					respJson = userApi.sendOtpToClient(user);
				}*/
				return new ResponseEntity<>(new ApiError(HttpStatus.OK, "OTP sent successfully"), HttpStatus.OK);
			}
		}catch (Exception e) {
				e.printStackTrace();
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user!");
			}

		throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user!");
	}

	@Override
	public ResponseEntity<?> validateOTP(LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		if (StringUtils.isBlank(loginDTO.getUsername()) )
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user!");
		User user = userRepository.findByUsername(loginDTO.getUsername());
		if (user == null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "User not found!invalid username");
			if (user != null && (user.getAuthority().contains(Authorities.SUPER_DISTRIBUTOR))
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				if (user.getMobileToken().equals(loginDTO.getOtp())) {
					return new ResponseEntity<>(new ApiError(HttpStatus.OK, "One Time Passowrd(OTP) Sent<br>Please enter it to complete verification."), HttpStatus.OK);
				} else {
					throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Invalid One Time Passowrd(OTP)!");
				}
			}
		throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid user!");
	}


}
