package com.viacarte.service.impl;

import java.io.IOException;
import java.util.*;

import com.viacarte.constant.UPayConstants;
import com.viacarte.dto.*;
import com.viacarte.dto.generic.KeyValueDTO;
import com.viacarte.entity.*;
import com.viacarte.enums.BizErrors;
import com.viacarte.enums.Status;
import com.viacarte.enums.UserType;
import com.viacarte.exception.BadRequestException;
import com.viacarte.exception.BizException;
import com.viacarte.repositories.*;
import com.viacarte.request.RequestJson;
import com.viacarte.response.ResponseJson;
import com.viacarte.service.ProcessorAPI;
import com.viacarte.util.Authorities;
import com.viacarte.util.CommonUtil;
import com.viacarte.util.CustomBcryptPasswordEncoder;
import com.viacarte.util.SecurityUtil;
import com.viacarte.validation.LoginValidation;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.guava.Sets;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class ProcessorImpl implements ProcessorAPI {
	private final IssuerRepository issuerRepository;
	private final KycRepository kycRepository;
	private final UserRepository userRepository;
	private final ClientDetailsRepository clientDetailsRepository;
	private final KYCProfileTypeRepository kycProfileTypeRepository;
	private final InviteCustomerRepository inviteCustomerRepository;
	private final KycCreatedLogsRepository kycCreatedLogsRepository;
	private final LoginValidation loginValidation;
	private final IssuersPIDRepository issuersPIDRepository;
	private final CardArrayRepository cardArrayRepository;
	private  final ProgramRepository programRepository;
	private final UserSessionRepository userSessionRepository;
	private final  AccountTypeRepository accountTypeRepository;
	private final AccountDetailRepository accountDetailRepository;

	
	public ProcessorImpl(IssuerRepository issuerRepository, KycRepository kycRepository, UserRepository userRepository,
						 ClientDetailsRepository clientDetailsRepository,
						 KYCProfileTypeRepository kycProfileTypeRepository,
						 InviteCustomerRepository inviteCustomerRepository,
						 KycCreatedLogsRepository kycCreatedLogsRepository, LoginValidation loginValidation, IssuersPIDRepository issuersPIDRepository, CardArrayRepository cardArrayRepository, ProgramRepository programRepository, UserSessionRepository userSessionRepository, AccountTypeRepository accountTypeRepository, AccountDetailRepository accountDetailRepository) {
		this.issuerRepository = issuerRepository;
		this.kycRepository = kycRepository;
		this.userRepository = userRepository;
		this.clientDetailsRepository = clientDetailsRepository;
		this.kycProfileTypeRepository = kycProfileTypeRepository;
		this.inviteCustomerRepository = inviteCustomerRepository;
		this.kycCreatedLogsRepository = kycCreatedLogsRepository;
		this.loginValidation = loginValidation;
		this.issuersPIDRepository = issuersPIDRepository;
		this.cardArrayRepository = cardArrayRepository;
		this.programRepository = programRepository;
		this.userSessionRepository = userSessionRepository;
		this.accountTypeRepository = accountTypeRepository;
		this.accountDetailRepository = accountDetailRepository;
	}


	@Override
	public ResponseJson createKycProfileType(KYCProfileTypeDTO kycProfileTypeDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		String error = loginValidation.validateKYCProfileType(kycProfileTypeDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		KYCProfileType profileType  = kycProfileTypeRepository.getByName(kycProfileTypeDTO.getProfileTypeName());
		if (profileType != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "profile type already exist!please enter new one");
		try{
			profileType = new KYCProfileType();
			profileType.setCurrency(kycProfileTypeDTO.getCurrency());
			profileType.setMonthlyLoadLimit(kycProfileTypeDTO.getMonthlyLoadLimit());
			profileType.setPoaCount(kycProfileTypeDTO.getPoaCount());
			profileType.setPoaDefinition(kycProfileTypeDTO.getPoaDefinition());
			profileType.setPoiCount(kycProfileTypeDTO.getPoiCount());
			profileType.setPoiDefinition(kycProfileTypeDTO.getPoiDefinition());
			profileType.setProfileTypeName(kycProfileTypeDTO.getProfileTypeName());
			profileType.setStatus(Status.Active);
			kycProfileTypeRepository.save(profileType);
			respJson.setMessage("kyc profile type added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"failed to update the kycprofiletype");
		}
	}

	@Override
	public ResponseJson listKycProfileTypes(HttpServletRequest request) {
		ResponseJson respJson = new ResponseJson();
		HttpSession userSession = request.getSession();
		if (StringUtils.isBlank(userSession.getId()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
		List<KYCProfileType> listProfileType = kycProfileTypeRepository.getKycProfileTypeList();
		respJson.setDetails(listProfileType);
		respJson.setSuccess(true);
		return respJson;
	}

	@Override
	public Object addIssuer(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			List<KYCProfileType> profileType = kycProfileTypeRepository.findAll();

			Map<String, List> map = new HashMap();
			map.put("kycProfileType", profileType);

			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting kycProfileType list");
		}
	}

	@Override
	public ResponseJson addIssuer(IssuerDTO issuerDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		String error = loginValidation.validateIssuer(issuerDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		Issuer issuer = issuerRepository.getIssuerByName(issuerDTO.getBankName());
		if (issuer != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Duplicate brand can not be inserted");
		try{
			issuer = new Issuer();
			issuer.setBankName(issuerDTO.getBankName());
			issuer.setBrand(issuerDTO.getBrand());
			issuer.setKycProfileType(issuerDTO.getIdType());
			issuer.setStatus(Status.Active);
			issuer.setIssuerHashId(UUID.randomUUID().toString());
			issuerRepository.save(issuer);
			respJson.setMessage("Issuer added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"failed to update the issuer");
		}
	}

	@Override
	public Object getIssuers(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			List<Issuer> issuerList = issuerRepository.findAllByStatus(Status.Active);
			Map<String, List> map = new HashMap();
			map.put("issuers", issuerList);

			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting kycProfileType list");
		}
	}


	@Override
	public ResponseJson createIssuersPID(IssuersPidDTO issuersPidDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		String error = loginValidation.validateIssuersPID(issuersPidDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		IssuersPID issuersPID = issuersPIDRepository.findAllByPid(issuersPidDTO.getPid());
		if (issuersPID != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Duplicate brand can not be inserted");
		try{
			issuersPID = new IssuersPID();
			issuersPID.setIssuerHashId(issuersPidDTO.getIssuerHashId());
			issuersPID.setIssuerName(issuersPidDTO.getIssuerName());
			issuersPID.setPid(issuersPidDTO.getPid());
			issuersPID.setStatus(Status.Active);
			issuersPIDRepository.save(issuersPID);
			respJson.setMessage("Issuer PID added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"failed to update the issuerPID");
		}
	}

	@Override
	public Object getIssuersPID(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			List<IssuersPID> issuersPIDList = issuersPIDRepository.findAllByStatus(Status.Active);
			Map<String, List> map = new HashMap();
			map.put("issuersPIDs", issuersPIDList);

			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting issuersPIDs list");
		}
	}

	@Override
	public Object getIssuersAndKycProfileTypes(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			Map<String, Object> map = new HashMap();
			Object issuerList = getIssuers(request);
			Object kycProfileTypes = addIssuer(request);
			map.put("issuerList", issuerList);
			map.put("kycProfileTypes", kycProfileTypes);
			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting issuersPIDs and kycProfileType list");
		}
	}

	@Override
	public ResponseJson addCardArray(CardArrayDTO cardArrayDTO, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ResponseJson respJson = new ResponseJson();
		String error = loginValidation.validateCardArray(cardArrayDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		CardArray cardArray = cardArrayRepository.findByCardName(cardArrayDTO.getCardName());
		IssuersPID issuersPID = issuersPIDRepository.findAllByPid(cardArrayDTO.getPid());
		Issuer issuer =  issuerRepository.findById(cardArrayDTO.getIssuerId());
		if (issuersPID == null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid program ID");
		if (issuer == null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "invalid issuer");
		if(cardArray!=null && cardArrayDTO.getCardArrayHashId() == null) {
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "cardArray already exist!please change the card name");
		}
		try{
			cardArray = cardArray != null ? cardArray : new CardArray();
			cardArray.setCardName(cardArrayDTO.getCardName());
			cardArray.setIssuer(issuer);
			cardArray.setCardIssuerPid(cardArrayDTO.getPid());
			cardArray.setProgramType(cardArrayDTO.getProgramType());
			cardArray.setMaximumBalanceLimit1(cardArrayDTO.getMaximumBalanceLimit1());
			cardArray.setMaximumBalanceLimit2(cardArrayDTO.getMaximumBalanceLimit2());
			cardArray.setBalanceLimit1(cardArrayDTO.getBalanceLimit1());
			cardArray.setBalanceLimit2(cardArrayDTO.getBalanceLimit2());
			cardArray.setLimits(cardArrayDTO.getLimits());
			cardArray.setCardArrayHashId(cardArrayDTO.getCardArrayHashId() != null ? cardArrayDTO.getCardArrayHashId()
					: CommonUtil.randomString(10, CommonUtil.CA));
			if (cardArrayDTO.getCardImage() != null) {
				cardArray.setCardImage(CommonUtil.uploadImage(cardArrayDTO.getCardImage(), cardArray.getCardArrayHashId(),
						"card_image", CommonUtil.cardImage_dir));
			}
			cardArray.setHeader1(cardArrayDTO.getHeader1());
			cardArray.setHeader2(cardArrayDTO.getHeader2());
			cardArray.setBenifitPoint1(cardArrayDTO.getBenifitPoint1());
			cardArray.setBenifitPoint2(cardArrayDTO.getBenifitPoint2());
			cardArray.setBenifitPoint3(cardArrayDTO.getBenifitPoint3());
			cardArray.setStatus(Status.Active);
			cardArrayRepository.save(cardArray);
			respJson.setMessage("card array added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"error!could not save card");
		}
	}

	@Override
	public Object getCardArray(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			Map<String, Object> map = new HashMap();
			List<CardArray> cardArrayList =  cardArrayRepository.findAllByStatus(Status.Active);
			map.put("cardArrayList", cardArrayList);
			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting cardArrayList");
		}
	}

	@Override
	public ResponseJson addProgram(ProgramDTO programDTO, HttpServletRequest request, HttpServletResponse response) {
		ResponseJson respJson = new ResponseJson();
		String error = loginValidation.validateProgram(programDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		Program program = programRepository.findByProgramName(programDTO.getProgramName());
		if(program!=null && programDTO.getProgramHashId() == null) {
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "cardArray already exist!please change the card name");
		}
		program = program != null ? program : new Program();
		try{
			program.setProgramName(programDTO.getProgramName());
			program.setProgramHashId(programDTO.getProgramHashId() != null ? programDTO.getProgramHashId()
					: CommonUtil.randomString(10, CommonUtil.PRGM));
			String rawToken = "" + System.currentTimeMillis() + program.getProgramHashId();
			program.setApiKey(program.getApiKey() != null ? program.getApiKey()
					: SecurityUtil.sha1(System.currentTimeMillis() + ""));
			program.setApiSecrete(program.getApiSecrete() != null ? program.getApiSecrete()
					: Base64.getEncoder().encodeToString(rawToken.getBytes()));
			if (programDTO.getTermsCondition() != null) {
				program.setTermsCondition(CommonUtil.uploadImage(programDTO.getTermsCondition(),
						program.getProgramHashId(), "termsCondition", CommonUtil.termsCondition_dir));
			}
			program.setStatus(Status.Active);
			List<Long> cardModuleIds = CommonUtil.commaSeparatedStringToList(programDTO.getCardModules());
			List<CardArray> cardArrayList =cardArrayRepository.findByIdIn(cardModuleIds);
			Set<CardArray> cardArraySet = Sets.newHashSet();
			cardArraySet.addAll(cardArrayList);
			 program.setProgramCards(cardArraySet);
			 programRepository.save(program);
			respJson.setMessage("program added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"error!could not save card");
		}
	}

	@Override
	public Object getModules(HttpServletRequest request) {
		return null;
	}

	@Override
	public Object getProgramList(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			Map<String, Object> map = new HashMap();
			List<Program> programList =  programRepository.findAllByStatus(Status.Active);
			List<KeyValueDTO> keyValueDTOList = new ArrayList<>();
			for(Program program :programList){
				KeyValueDTO keyValueDTO = new KeyValueDTO();
				keyValueDTO.setId(program.getId());
				keyValueDTO.setName(program.getProgramName());
				keyValueDTOList.add(keyValueDTO);
			}
			map.put("programList", keyValueDTOList);
			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting programList");
		}
	}

	@Override
	public ResponseJson addClientDetail(ClientDetailDTO clientDetailDTO, HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		if (StringUtils.isBlank(session.getId()))
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
		UserSession userSession = userSessionRepository.findBySessionId(session.getId());
		if(userSession == null){
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session error");
		}
		String error = loginValidation.validateClientDetails(clientDetailDTO);
		if (error != null)
			throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), error);
		try{
			List<User> users = userRepository.findByEmail(clientDetailDTO.getPrimaryEmail());

			if (users != null && !users.isEmpty()) {
				for (User u : users) {
					if (u != null && u.isFullyFilled() && u.getEmail().equals(clientDetailDTO.getPrimaryEmail())) {
						throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Primary email already exist");
					}
				}
			}

			List<User> userList = userRepository.findByContactNo(clientDetailDTO.getPrimaryContact());
			if (userList != null && !userList.isEmpty()) {
				for (User u : userList) {
					if (u != null && u.isFullyFilled() && u.getContactNo().equals(clientDetailDTO.getPrimaryContact())) {
						throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "Primary contact already exist");
					}
				}
			}
			User user = new User();
			user.setAuthority(Authorities.CLIENT + "," + Authorities.AUTHENTICATED);
			user.setMobileStatus(Status.Active);
			user.setMobileToken("123456");
			// user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setAddress(clientDetailDTO.getAddress());
			user.setEmail(clientDetailDTO.getPrimaryEmail());
			user.setUsername(clientDetailDTO.getPrimaryEmail());
			user.setUserHashId(CommonUtil.randomString(8, CommonUtil.CLNT));
			user.setChannelType(UserType.Client);
			user.setUserType(UserType.Client);
			user.setChannelType(UserType.Master_Admin);
			user.setPassword(CustomBcryptPasswordEncoder.getBcryptPasswordEncoder().encode(clientDetailDTO.getPrimaryContact()));
			user.setContactNo(clientDetailDTO.getPrimaryContact());
			user.setChannelId(userSession.getUser().getUserHashId());
			user.setCountry(clientDetailDTO.getCountry());
			String resultError = checkClientValidation(user, clientDetailDTO);
			if(resultError == null) {
				userRepository.save(user);
				AccountDetails pqAccountDetails = createAccount(user, "KYC");
				if (pqAccountDetails != null) {
					user.setAccountDetail(pqAccountDetails);
					userRepository.save(user);
					// smsSenderApi.sendUserSMS(SMSTemplete.MOBILE_VERIFICATION, user);
				}
			}else {
				throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),resultError);
			}
			return onboardClient(user, clientDetailDTO);
		}catch(Exception e){
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"error!could not save card");
		}
	}

	@Override
	public Object getClientList(HttpServletRequest request) {
		try {
			ResponseJson respJson = new ResponseJson();
			HttpSession userSession = request.getSession();
			if (StringUtils.isBlank(userSession.getId()))
				throw new BadRequestException(BizErrors.BAD_REQUEST_ERROR.getValue(), "session expired");
			Map<String, Object> map = new HashMap();
			List<ClientDetails> clientDetails = clientDetailsRepository.findAll();
			map.put("clientList", clientDetails);
			return map;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(), "error in getting clients");
		}
	}

	private String checkClientValidation(User user, ClientDetailDTO request) {
		try {
			ClientDetails clientDetails;
			clientDetails = clientDetailsRepository.findByEntityName(request.getEntityName());
			clientDetails = clientDetails != null ? clientDetails : new ClientDetails();

			clientDetails.setBillingEmail(request.getBillingEmail());
			clientDetails.setBillingName(request.getBillingName());
			clientDetails.setBillingPhone(request.getBillingPhone());

			clientDetails.setClientHashId(user.getUserHashId());
			clientDetails.setAddress(request.getAddress());
			try {
				clientDetails.setPrimaryContact(request.getPrimaryContact());
				clientDetailsRepository.save(clientDetails);
			} catch (DataIntegrityViolationException e) {
				throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"Error!duplicate entry for primary contact number");
			}
			try {
				clientDetails.setEntityName(request.getEntityName());
				clientDetailsRepository.save(clientDetails);
			} catch (DataIntegrityViolationException e) {
				throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"Error!duplicate entry for company name");
			}

			try {
				clientDetails.setPrimaryEmail(request.getPrimaryEmail());
				clientDetailsRepository.save(clientDetails);
			} catch (DataIntegrityViolationException e) {
				throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"Error!duplicate entry for primary email id");
			}

			clientDetails.setPrimaryName(request.getPrimaryName());
			clientDetails.setStatus(Status.Active);
			if(request.getClientLogo().getSize() >0) {
				clientDetails.setClientLogo(CommonUtil.uploadImage(request.getClientLogo(), user.getUserHashId(), "client_logo",
						CommonUtil.clientLogo_dir));
			}
			clientDetails.setColorCode(request.getColorCode());
			clientDetails.setClientUrlId(getClientURL(request.getEntityName()));
			clientDetailsRepository.save(clientDetails);
			//sendEmail(clientDetails);
			return null;
		} catch (DataIntegrityViolationException e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"client onboarding failed!");
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"client onboarding failed!");
		}
	}

	private String getClientURL(String entityname) {
		entityname = entityname.replace(" ", "");
		entityname = "in" + (entityname.length() > 5 ? entityname.substring(0, 5) : entityname);
		return entityname.toLowerCase();
	}

	private AccountDetails createAccount(User user, String code) {
		AccountDetails pqAccountDetail = user.getAccountDetail() != null ? user.getAccountDetail()
				: new AccountDetails();
		try {
			pqAccountDetail.setBalance(pqAccountDetail.getBalance());
			pqAccountDetail.setAccountType(accountTypeRepository.findByCode(code));
			accountDetailRepository.save(pqAccountDetail);
			return pqAccountDetail;
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"createAccount for client onboarding failed!");
		}
	}

	private ResponseJson onboardClient(User user, ClientDetailDTO request) {
		ResponseJson respJson = new ResponseJson();
		try {
			ClientDetails clientDetails = clientDetailsRepository.findByClientHashId(user.getUserHashId());
			clientDetails.setAccountDetails(user.getAccountDetail());
			String rawToken = "" + System.currentTimeMillis() + user.getAccountDetail().getAccountNumber();
			clientDetails.setApiKey(SecurityUtil.sha1(System.currentTimeMillis() + ""));
			clientDetails.setApiToken(Base64.getEncoder().encodeToString(rawToken.getBytes()));

			List<Long> programIds = CommonUtil.commaSeparatedStringToList(request.getProgramIds());
			List<Program> programList =programRepository.findByIdIn(programIds);
			Set<Program> programSet = Sets.newHashSet();
			programSet.addAll(programList);
			clientDetails.setClientPrograms(programSet);
			clientDetailsRepository.save(clientDetails);
			//sendEmail(clientDetails);
			respJson.setMessage("client added successfully");
			respJson.setCode( HttpStatus.OK);
			respJson.setSuccess(true);
			return respJson;
		} catch (DataIntegrityViolationException e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"client onboarding failed!");
		} catch (Exception e) {
			throw new BizException(BizErrors.BAD_REQUEST_ERROR.getValue(),"client onboarding failed!");
		}
	}

	/*private void sendEmail(ClientDetails clientDetails) {

		EmailSender.emailSender(clientDetails.getPrimaryEmail(), "Dear " + clientDetails.getPrimaryName()
				+ "\n\nYou have been invited into the VIaCarte Admin Portal. Please follow"
				+ " the link below and set your password to login into the system.\n\nURL : "+ UPayConstants.getUrl()+"/"
				+ clientDetails.getClientUrlId() + "/Master/Home\nUsername : " + clientDetails.getPrimaryEmail()
				+ "\nWelcome aboard!", "Login Credentials");

	}*/

}
