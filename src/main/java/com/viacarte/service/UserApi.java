package com.viacarte.service;

import com.viacarte.dto.LoginDTO;
import com.viacarte.entity.User;
import com.viacarte.response.ResponseJson;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface UserApi  {

	User findByUserName(String name);

	ResponseEntity<?> sendOTP(LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response);

    ResponseEntity<?> validateOTP(LoginDTO loginDTO, HttpServletRequest request, HttpServletResponse response);
}

