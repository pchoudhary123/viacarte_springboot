package com.viacarte.enums;

public enum UserType {

    Master_Admin, Distributor, Admin, Agent, User, Locked, Support, Complaint, KYC_Approver, Accounts, Client, KYC;
}