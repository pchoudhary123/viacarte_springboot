package com.viacarte.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "KycDetailLog")
public class KycDetailLog extends AbstractEntity<Long> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String idType;
    private String idNumber;
    private String languageId;
    private String idProofCountry;
    private String issueDateIdProof;
    private String expiryDateIdProof;
    private String idProofFilePathSrc;
    private String idProofFilePathBackSrc;
    private String addressProofType;
    private String addressIssuedBy;
    private String issueDateAddressProof;
    private String addressDocLanguage;
    private String addressProofFilePathSrc;
    private String addressProofFilePathBackSrc;
    private String clientMailId;
    private String rejectionReason;
    private String lastModifiedBy;
    private boolean kycStatus = false;






    public boolean isKycStatus() {
        return kycStatus;
    }
    public void setKycStatus(boolean kycStatus) {
        this.kycStatus = kycStatus;
    }
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
    public String getIdProofFilePathBackSrc() {
        return idProofFilePathBackSrc;
    }
    public void setIdProofFilePathBackSrc(String idProofFilePathBackSrc) {
        this.idProofFilePathBackSrc = idProofFilePathBackSrc;
    }
    public String getAddressProofFilePathBackSrc() {
        return addressProofFilePathBackSrc;
    }
    public void setAddressProofFilePathBackSrc(String addressProofFilePathBackSrc) {
        this.addressProofFilePathBackSrc = addressProofFilePathBackSrc;
    }
    public String getClientMailId() {
        return clientMailId;
    }
    public void setClientMailId(String clientMailId) {
        this.clientMailId = clientMailId;
    }
    public String getRejectionReason() {
        return rejectionReason;
    }
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    public String getAddress2() {
        return address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getIdType() {
        return idType;
    }
    public void setIdType(String idType) {
        this.idType = idType;
    }
    public String getIdNumber() {
        return idNumber;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    public String getLanguageId() {
        return languageId;
    }
    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }
    public String getIdProofCountry() {
        return idProofCountry;
    }
    public void setIdProofCountry(String idProofCountry) {
        this.idProofCountry = idProofCountry;
    }
    public String getIssueDateIdProof() {
        return issueDateIdProof;
    }
    public void setIssueDateIdProof(String issueDateIdProof) {
        this.issueDateIdProof = issueDateIdProof;
    }
    public String getExpiryDateIdProof() {
        return expiryDateIdProof;
    }
    public void setExpiryDateIdProof(String expiryDateIdProof) {
        this.expiryDateIdProof = expiryDateIdProof;
    }

    public String getAddressProofType() {
        return addressProofType;
    }
    public void setAddressProofType(String addressProofType) {
        this.addressProofType = addressProofType;
    }
    public String getAddressIssuedBy() {
        return addressIssuedBy;
    }
    public void setAddressIssuedBy(String addressIssuedBy) {
        this.addressIssuedBy = addressIssuedBy;
    }
    public String getIssueDateAddressProof() {
        return issueDateAddressProof;
    }
    public void setIssueDateAddressProof(String issueDateAddressProof) {
        this.issueDateAddressProof = issueDateAddressProof;
    }
    public String getAddressDocLanguage() {
        return addressDocLanguage;
    }
    public void setAddressDocLanguage(String addressDocLanguage) {
        this.addressDocLanguage = addressDocLanguage;
    }


    public String getIdProofFilePathSrc() {
        return idProofFilePathSrc;
    }
    public void setIdProofFilePathSrc(String idProofFilePathSrc) {
        this.idProofFilePathSrc = idProofFilePathSrc;
    }
    public String getAddressProofFilePathSrc() {
        return addressProofFilePathSrc;
    }
    public void setAddressProofFilePathSrc(String addressProofFilePathSrc) {
        this.addressProofFilePathSrc = addressProofFilePathSrc;
    }




}
