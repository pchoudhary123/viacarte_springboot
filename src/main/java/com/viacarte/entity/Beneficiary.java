package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Beneficiary")
public class Beneficiary extends AbstractEntity<Long> {

    /**
     *
     */
    private static final long serialVersionUID = 7374596811130929761L;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;
    @Column
    private String beneficiaryemail;
    @Column
    private String beneficiaryname;
    @Column
    private String beneficiaryphonenumber;
    @Column
    private String userHashId;
    @Column
    private String  beneficiaryUserImage;




    public String getBeneficiaryUserImage() {
        return beneficiaryUserImage;
    }

    public void setBeneficiaryUserImage(String beneficiaryUserImage) {
        this.beneficiaryUserImage = beneficiaryUserImage;
    }

    public String getUserHashId() {
        return userHashId;
    }

    public void setUserHashId(String userHashId) {
        this.userHashId = userHashId;
    }

    public String getBeneficiaryemail() {
        return beneficiaryemail;
    }

    public void setBeneficiaryemail(String beneficiaryemail) {
        this.beneficiaryemail = beneficiaryemail;
    }

    @Enumerated(EnumType.STRING)
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getBeneficiaryname() {
        return beneficiaryname;
    }

    public void setBeneficiaryname(String beneficiaryname) {
        this.beneficiaryname = beneficiaryname;
    }

    public String getBeneficiaryphonenumber() {
        return beneficiaryphonenumber;
    }

    public void setBeneficiaryphonenumber(String beneficiaryphonenumber) {
        this.beneficiaryphonenumber = beneficiaryphonenumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
