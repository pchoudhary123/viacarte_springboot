package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;


@Entity
@Table(name = "InviteCustomer")
public class InviteCustomer extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4512450918609860064L;

	@Column(nullable = false)
	private String customerContact;

	@Column(nullable = false)
	private String clientHashId;

	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String invitationCode;

	private String message;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClientHashId() {
		return clientHashId;
	}

	public void setClientHashId(String clientHashId) {
		this.clientHashId = clientHashId;
	}
	
	
}
