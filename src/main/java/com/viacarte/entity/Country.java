package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;


@Table(name="Country")
@Entity
public class Country extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2026996262065125663L;
	private String name;
	private String isoCode;
	private String countryCode;
	private String callingCode;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	public String getCallingCode() {
		return callingCode;
	}

	public void setCallingCode(String callingCode) {
		this.callingCode = callingCode;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
