package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;



@Entity
@Table(name = "Issuer")
public class Issuer extends AbstractEntity<Long> {
    /**
     *
     */

    @Column(nullable = false)
    private String bankName;

    @Column(nullable = false, unique = true)
    private String brand;

    private String kycProfileType;
    @Column(nullable = false)

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(nullable = false, unique = true)
    private String issuerHashId;

    public String getIssuerHashId() {
        return issuerHashId;
    }

    public void setIssuerHashId(String issuerHashId) {
        this.issuerHashId = issuerHashId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getKycProfileType() {
        return kycProfileType;
    }

    public void setKycProfileType(String kycProfileType) {
        this.kycProfileType = kycProfileType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
