package com.viacarte.entity;

import com.viacarte.enums.Status;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Table(name = "KycDetail")
@Entity
public class KycDetail extends AbstractEntity<Long> {

    private static final long serialVersionUID = 2531830092010494854L;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    private String rejectReason;
    private Boolean kycSubmitted;
    private String idProofType;
    private String idProofNumber;
    private String languageId;
    private String idProofCountry;
    @Column
    @Temporal(TemporalType.DATE)
    private Date expiryDateIdProof;
    @Column
    @Temporal(TemporalType.DATE)
    private Date issueDateIdProof;
    private String idProofFilePath;
    private String idProofFilePathBack;
    private String addressProofType;
    private String addressIssuedBy;
    @Column
    @Temporal(TemporalType.DATE)
    private Date issueDateAddressProof;
    private String addressDocLanguage;
    private String addressProofFilePath;
    private String addressProofFilePathBack;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String postalCode;
    private String countryCode;
    private String expireDocStatus;
    private Long listOfIdProofId;

    @Column
    @Enumerated(EnumType.STRING)
    private Status kycStatus;

    private String verificationCode;

    @OneToOne(fetch= FetchType.EAGER)
    private KYCParametersStatus kycParametersStatus;

    @OneToOne(fetch= FetchType.EAGER)
    private KycDetailLog kycDetailLog;


    public Long getListOfIdProofId() {
        return listOfIdProofId;
    }

    public void setListOfIdProofId(Long listOfIdProofId) {
        this.listOfIdProofId = listOfIdProofId;
    }

    public String getExpireDocStatus() {
        return expireDocStatus;
    }

    public void setExpireDocStatus(String expireDocStatus) {
        this.expireDocStatus = expireDocStatus;
    }

    public KycDetailLog getKycDetailLog() {
        return kycDetailLog;
    }

    public void setKycDetailLog(KycDetailLog kycDetailLog) {
        this.kycDetailLog = kycDetailLog;
    }

    public String getIdProofFilePathBack() {
        return idProofFilePathBack;
    }

    public void setIdProofFilePathBack(String idProofFilePathBack) {
        this.idProofFilePathBack = idProofFilePathBack;
    }

    public String getAddressProofFilePathBack() {
        return addressProofFilePathBack;
    }

    public void setAddressProofFilePathBack(String addressProofFilePathBack) {
        this.addressProofFilePathBack = addressProofFilePathBack;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Status getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(Status kycStatus) {
        this.kycStatus = kycStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Boolean getKycSubmitted() {
        return kycSubmitted;
    }

    public void setKycSubmitted(Boolean kycSubmitted) {
        this.kycSubmitted = kycSubmitted;
    }

    public String getIdProofType() {
        return idProofType;
    }

    public void setIdProofType(String idProofType) {
        this.idProofType = idProofType;
    }

    public String getIdProofNumber() {
        return idProofNumber;
    }

    public void setIdProofNumber(String idProofNumber) {
        this.idProofNumber = idProofNumber;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getIdProofCountry() {
        return idProofCountry;
    }

    public void setIdProofCountry(String idProofCountry) {
        this.idProofCountry = idProofCountry;
    }

    public Date getExpiryDateIdProof() {
        return expiryDateIdProof;
    }

    public void setExpiryDateIdProof(Date expiryDateIdProof) {
        this.expiryDateIdProof = expiryDateIdProof;
    }

    public Date getIssueDateIdProof() {
        return issueDateIdProof;
    }

    public void setIssueDateIdProof(Date issueDateIdProof) {
        this.issueDateIdProof = issueDateIdProof;
    }

    public String getIdProofFilePath() {
        return idProofFilePath;
    }

    public void setIdProofFilePath(String idProofFilePath) {
        this.idProofFilePath = idProofFilePath;
    }

    public String getAddressProofType() {
        return addressProofType;
    }

    public void setAddressProofType(String addressProofType) {
        this.addressProofType = addressProofType;
    }

    public String getAddressIssuedBy() {
        return addressIssuedBy;
    }

    public void setAddressIssuedBy(String addressIssuedBy) {
        this.addressIssuedBy = addressIssuedBy;
    }

    public Date getIssueDateAddressProof() {
        return issueDateAddressProof;
    }

    public void setIssueDateAddressProof(Date issueDateAddressProof) {
        this.issueDateAddressProof = issueDateAddressProof;
    }

    public String getAddressDocLanguage() {
        return addressDocLanguage;
    }

    public void setAddressDocLanguage(String addressDocLanguage) {
        this.addressDocLanguage = addressDocLanguage;
    }

    public String getAddressProofFilePath() {
        return addressProofFilePath;
    }

    public void setAddressProofFilePath(String addressProofFilePath) {
        this.addressProofFilePath = addressProofFilePath;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public KYCParametersStatus getKycParametersStatus() {
        return kycParametersStatus;
    }

    public void setKycParametersStatus(KYCParametersStatus kycParametersStatus) {
        this.kycParametersStatus = kycParametersStatus;
    }

}
