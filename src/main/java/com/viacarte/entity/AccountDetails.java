package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Table(name = "AccountDetails")
@Entity
public class AccountDetails extends AbstractEntity<Long> {

    private static final long serialVersionUID = 3382315753571634526L;

    private double balance;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private AccountType accountType;

    @Column(unique = true)
    private String accountNumber;

    @Enumerated(EnumType.STRING)
    private Status status;

    public AccountDetails() {
        setAccountNumber((long) (Math.random() * 100000000000000L) + "");
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

}
