package com.viacarte.entity;


import com.viacarte.enums.Status;
import com.viacarte.enums.UserType;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Table(name = "User")
@Entity
public class User extends AbstractEntity<Long> {

    private static final long serialVersionUID = 8453654076725018243L;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = true)
    private String password;

    @Column(nullable = false)
    private UserType userType;

    @Column(nullable = false)
    private String authority;

    @Column
    @Enumerated(EnumType.STRING)
    private Status mobileStatus;

    @Column(nullable = true)
    private String firstName;

    @Column(nullable = true)
    private String lastName;

    @Column
    private String address;

    @Column(nullable = false)
    private String contactNo;

    @Column(nullable = true)
    private String email;

    @Column
    private String mpin;

    @Column
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @Column(nullable = true)
    private String idType;

    @Column(nullable = true)
    private String idNo;

    @Column
    private String gender;

    @Column
    private String city;

    @Column
    private String state;

    @Column
    private String postalCode;

    @Column
    private String country;

    @Column
    private String emailToken;

    @Column
    private String mobileToken;

    @Lob
    private String gcmId;

    @Column(nullable = false, unique = true)
    private String userHashId;

    @Column(nullable = true)
    private String androidDeviceID;

    @Column
    private boolean isFullyFilled;

    @OneToOne(fetch = FetchType.EAGER)
    private AccountDetails accountDetail;



    @Enumerated(EnumType.STRING)
    private UserType channelType;

    @Column(nullable = false)
    private String channelId;

    @Column
    private String image;

    private String channelName;

    private long question1;
    private long question2;
    private long question3;
    private String answer1;
    private String answer2;
    private String answer3;
    private Date lastLogin;

    private long progressBar = 20;
    private String nickName;
    private String communication;
    private String twitterAccount;
    private String facebookAccount;
    private String languageUser;
    private String timeZone;
    private String title;
    private String suffix;
    private Status expiryId;




    public Status getExpiryId() {
        return expiryId;
    }

    public void setExpiryId(Status expiryId) {
        this.expiryId = expiryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public String getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(String facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public String getLanguageUser() {
        return languageUser;
    }

    public void setLanguageUser(String languageUser) {
        this.languageUser = languageUser;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public long getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(long progressBar) {
        this.progressBar = progressBar;
    }

    public long getQuestion1() {
        return question1;
    }

    public void setQuestion1(long question1) {
        this.question1 = question1;
    }

    public long getQuestion2() {
        return question2;
    }

    public void setQuestion2(long question2) {
        this.question2 = question2;
    }

    public long getQuestion3() {
        return question3;
    }

    public void setQuestion3(long question3) {
        this.question3 = question3;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public UserType getChannelType() {
        return channelType;
    }

    public void setChannelType(UserType channelType) {
        this.channelType = channelType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean isFullyFilled() {
        return isFullyFilled;
    }

    public void setFullyFilled(boolean isFullyFilled) {
        this.isFullyFilled = isFullyFilled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Status getMobileStatus() {
        return mobileStatus;
    }

    public void setMobileStatus(Status mobileStatus) {
        this.mobileStatus = mobileStatus;
    }

    public AccountDetails getAccountDetail() {
        return accountDetail;
    }

    public void setAccountDetail(AccountDetails accountDetail) {
        this.accountDetail = accountDetail;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getMobileToken() {
        return mobileToken;
    }

    public void setMobileToken(String mobileToken) {
        this.mobileToken = mobileToken;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getAndroidDeviceID() {
        return androidDeviceID;
    }

    public void setAndroidDeviceID(String androidDeviceID) {
        this.androidDeviceID = androidDeviceID;
    }

    public String getUserHashId() {
        return userHashId;
    }

    public void setUserHashId(String userHashId) {
        this.userHashId = userHashId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMpin() {
        return mpin;
    }

    public void setMpin(String mpin) {
        this.mpin = mpin;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}