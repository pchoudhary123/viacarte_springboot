package com.viacarte.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.viacarte.enums.Status;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CardArray")
public class CardArray extends AbstractEntity<Long> {

    @Column(name = "card_name", nullable = false, unique = true)
    private String cardName;
    @Column(name = "card_array_hash_id", nullable = false, unique = true)
    private String cardArrayHashId;
    @Column(name = "header1", nullable = false)
    private String header1;
    @Column(name = "header2", nullable = false)
    private String header2;
    @Column(name = "benifit_point1", nullable = false)
    private String benifitPoint1;
    @Column(name = "benifit_point2", nullable = false)
    private String benifitPoint2;
    @Column(name = "benifit_point3", nullable = false)
    private String benifitPoint3;
    @Column(name = "card_image", nullable = false)
    private String cardImage;
    @Column(name = "currency", nullable = false)
    private String currency;

    @Column(name = "card_issuer_pid", nullable = false)
    private String cardIssuerPid;
    @Column(name = "program_type", nullable = false)
    private String programType;

    @Column(name = "maximum_balance_limit1", nullable = false)
    private Double maximumBalanceLimit1;
    @Column(name = "balance_limit1", nullable = false)
    private Double BalanceLimit1;
    @Column(name = "maximum_balance_limit2")
    private Double MaximumBalanceLimit2;
    @Column(name = "balance_limit2")
    private Double BalanceLimit2;
    @Column(name = "limits", nullable = false)
    private int Limits;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToOne(fetch=FetchType.EAGER)
    private Issuer issuer;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToMany(mappedBy = "programCards")
    private Set<Program> program = new HashSet<>();

    public Set<Program> getProgram() {
        return program;
    }

    public void setProgram(Set<Program> program) {
        this.program = program;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardArrayHashId() {
        return cardArrayHashId;
    }

    public void setCardArrayHashId(String cardArrayHashId) {
        this.cardArrayHashId = cardArrayHashId;
    }

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }

    public String getBenifitPoint1() {
        return benifitPoint1;
    }

    public void setBenifitPoint1(String benifitPoint1) {
        this.benifitPoint1 = benifitPoint1;
    }

    public String getBenifitPoint2() {
        return benifitPoint2;
    }

    public void setBenifitPoint2(String benifitPoint2) {
        this.benifitPoint2 = benifitPoint2;
    }

    public String getBenifitPoint3() {
        return benifitPoint3;
    }

    public void setBenifitPoint3(String benifitPoint3) {
        this.benifitPoint3 = benifitPoint3;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getCardIssuerPid() {
        return cardIssuerPid;
    }

    public void setCardIssuerPid(String cardIssuerPid) {
        this.cardIssuerPid = cardIssuerPid;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public Double getMaximumBalanceLimit1() {
        return maximumBalanceLimit1;
    }

    public void setMaximumBalanceLimit1(Double maximumBalanceLimit1) {
        this.maximumBalanceLimit1 = maximumBalanceLimit1;
    }


    public Double getMaximumBalanceLimit2() {
        return MaximumBalanceLimit2;
    }

    public void setMaximumBalanceLimit2(Double maximumBalanceLimit2) {
        MaximumBalanceLimit2 = maximumBalanceLimit2;
    }

    public Double getBalanceLimit1() {
        return BalanceLimit1;
    }

    public void setBalanceLimit1(Double balanceLimit1) {
        BalanceLimit1 = balanceLimit1;
    }

    public Double getBalanceLimit2() {
        return BalanceLimit2;
    }

    public void setBalanceLimit2(Double balanceLimit2) {
        BalanceLimit2 = balanceLimit2;
    }

    public int getLimits() {
        return Limits;
    }

    public void setLimits(int limits) {
        Limits = limits;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
