package com.viacarte.entity;

import com.viacarte.enums.Status;
import org.glassfish.jersey.internal.guava.Sets;

import javax.persistence.*;
import java.util.Set;


@Table(name = "Client_details")
@Entity
public class ClientDetails extends AbstractEntity<Long> {

    /**
     *
     */
    @Column(name = "api_key", unique = true)
    private String apiKey;

    @Column(name = "api_token", unique = true)
    private String apiToken;

    @OneToOne
    private AccountDetails accountDetails;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "client_hash_id" ,nullable = false, unique = true)
    private String clientHashId;

    @Column(name="entity_name",unique = true)
    private String entityName;
    @Column(name="primary_name")
    private String primaryName;
    @Column(name="primary_contact",unique = true)
    private String primaryContact;
    @Column(name="primary_email", unique = true)
    private String primaryEmail;
    @Column(name="billing_name")
    private String billingName;
    @Column(name="billing_phone")
    private String billingPhone;
    @Column(name="billing_email")
    private String billingEmail;
    @Column(name="client_logo")
    private String clientLogo;
    @Column(name="color_code")
    private String colorCode;

    @Column(name="client_url_id", unique = true)
    private String clientUrlId;
    @Column(name="address")
    private String address;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "client_has_program", joinColumns = {@JoinColumn(name = "Client_details_id")},
            inverseJoinColumns = {@JoinColumn(name = "program_id")})
    private Set<Program> clientPrograms = Sets.newHashSet();

    public Set<Program> getClientPrograms() {
        return clientPrograms;
    }

    public void setClientPrograms(Set<Program> clientPrograms) {
        this.clientPrograms = clientPrograms;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getClientHashId() {
        return clientHashId;
    }

    public void setClientHashId(String clientHashId) {
        this.clientHashId = clientHashId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getClientLogo() {
        return clientLogo;
    }

    public void setClientLogo(String clientLogo) {
        this.clientLogo = clientLogo;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getClientUrlId() {
        return clientUrlId;
    }

    public void setClientUrlId(String clientUrlId) {
        this.clientUrlId = clientUrlId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
