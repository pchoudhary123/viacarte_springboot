package com.viacarte.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.viacarte.enums.Status;
import org.glassfish.jersey.internal.guava.Sets;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Program")
public class Program extends AbstractEntity<Long>{

    @Column(name = "program_name", nullable = false, unique = true)
    private String programName;

    @Column(name = "program_hash_id", nullable = false, unique = true)
    private String programHashId;


    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "api_key",nullable = false, unique = true)
    private String apiKey;

    @Column(name = "api_secrete",nullable = false, unique = true)
    private String apiSecrete;

    @Column(name = "terms_condition",nullable = false)
    private String termsCondition;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToMany(mappedBy = "clientPrograms")
    private Set<ClientDetails> clientDetails = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "program_has_card", joinColumns = {@JoinColumn(name = "program_id")},
            inverseJoinColumns = {@JoinColumn(name = "card_array_id")})
    private Set<CardArray> programCards = Sets.newHashSet();

    public Set<CardArray> getProgramCards() {
        return programCards;
    }

    public void setProgramCards(Set<CardArray> programCards) {
        this.programCards = programCards;
    }

    public String getTermsCondition() {
        return termsCondition;
    }

    public void setTermsCondition(String termsCondition) {
        this.termsCondition = termsCondition;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramHashId() {
        return programHashId;
    }

    public void setProgramHashId(String programHashId) {
        this.programHashId = programHashId;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecrete() {
        return apiSecrete;
    }

    public void setApiSecrete(String apiSecrete) {
        this.apiSecrete = apiSecrete;
    }

    public Set<ClientDetails> getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(Set<ClientDetails> clientDetails) {
        this.clientDetails = clientDetails;
    }
}
