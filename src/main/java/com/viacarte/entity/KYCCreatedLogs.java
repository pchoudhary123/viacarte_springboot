package com.viacarte.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "KycCreatedLogs")
public class KYCCreatedLogs extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String idType;
	private String idNumber;
	private String languageId;
	private String idProofCountry;
	private Date issueDateIdProof;
	private Date expiryDateIdProof;
	private String idProofFilePathSrc;
	private String idProofFilePathBackSrc;
	private String addressProofType;
	private String addressIssuedBy;
	private Date issueDateAddressProof;
	private String addressDocLanguage;
	private String addressProofFilePathSrc;
	private String addressProofFilePathBackSrc;
	private String clientMailId;
	private String rejectionReason;
	private String lastModifiedBy;
	private String firstName_radio;
	private String lastName_radio;
	private String address1_radio;
	private String address2_radio;
	private String city_radio;
	private String state_radio;
	private String country_radio;
	private String postalCode_radio;
	private String idType_radio;
	private String idNumber_radio;
	private String languageId_radio;
	private String idProofCountry_radio;
	private String issueDateIdProof_radio;
	private String expiryDateIdProof_radio;
	private String idProofFilePath_radio;
	private String addressProofType_radio;
	private String addressIssuedBy_radio;
	private String issueDateAddressProof_radio;
	private String addressDocLanguage_radio;
	private String addressProofFilePath_radio;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getIdProofCountry() {
		return idProofCountry;
	}
	public void setIdProofCountry(String idProofCountry) {
		this.idProofCountry = idProofCountry;
	}
	public Date getIssueDateIdProof() {
		return issueDateIdProof;
	}
	public void setIssueDateIdProof(Date date) {
		this.issueDateIdProof = date;
	}
	public Date getExpiryDateIdProof() {
		return expiryDateIdProof;
	}
	public void setExpiryDateIdProof(Date date) {
		this.expiryDateIdProof = date;
	}
	public String getIdProofFilePathSrc() {
		return idProofFilePathSrc;
	}
	public void setIdProofFilePathSrc(String idProofFilePathSrc) {
		this.idProofFilePathSrc = idProofFilePathSrc;
	}
	public String getIdProofFilePathBackSrc() {
		return idProofFilePathBackSrc;
	}
	public void setIdProofFilePathBackSrc(String idProofFilePathBackSrc) {
		this.idProofFilePathBackSrc = idProofFilePathBackSrc;
	}
	public String getAddressProofType() {
		return addressProofType;
	}
	public void setAddressProofType(String addressProofType) {
		this.addressProofType = addressProofType;
	}
	public String getAddressIssuedBy() {
		return addressIssuedBy;
	}
	public void setAddressIssuedBy(String addressIssuedBy) {
		this.addressIssuedBy = addressIssuedBy;
	}
	public Date getIssueDateAddressProof() {
		return issueDateAddressProof;
	}
	public void setIssueDateAddressProof(Date date) {
		this.issueDateAddressProof = date;
	}
	public String getAddressDocLanguage() {
		return addressDocLanguage;
	}
	public void setAddressDocLanguage(String addressDocLanguage) {
		this.addressDocLanguage = addressDocLanguage;
	}
	public String getAddressProofFilePathSrc() {
		return addressProofFilePathSrc;
	}
	public void setAddressProofFilePathSrc(String addressProofFilePathSrc) {
		this.addressProofFilePathSrc = addressProofFilePathSrc;
	}
	public String getAddressProofFilePathBackSrc() {
		return addressProofFilePathBackSrc;
	}
	public void setAddressProofFilePathBackSrc(String addressProofFilePathBackSrc) {
		this.addressProofFilePathBackSrc = addressProofFilePathBackSrc;
	}
	public String getClientMailId() {
		return clientMailId;
	}
	public void setClientMailId(String clientMailId) {
		this.clientMailId = clientMailId;
	}
	public String getRejectionReason() {
		return rejectionReason;
	}
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public String getFirstName_radio() {
		return firstName_radio;
	}
	public void setFirstName_radio(String firstName_radio) {
		this.firstName_radio = firstName_radio;
	}
	public String getLastName_radio() {
		return lastName_radio;
	}
	public void setLastName_radio(String lastName_radio) {
		this.lastName_radio = lastName_radio;
	}
	public String getAddress1_radio() {
		return address1_radio;
	}
	public void setAddress1_radio(String address1_radio) {
		this.address1_radio = address1_radio;
	}
	public String getAddress2_radio() {
		return address2_radio;
	}
	public void setAddress2_radio(String address2_radio) {
		this.address2_radio = address2_radio;
	}
	public String getCity_radio() {
		return city_radio;
	}
	public void setCity_radio(String city_radio) {
		this.city_radio = city_radio;
	}
	public String getState_radio() {
		return state_radio;
	}
	public void setState_radio(String state_radio) {
		this.state_radio = state_radio;
	}
	public String getCountry_radio() {
		return country_radio;
	}
	public void setCountry_radio(String country_radio) {
		this.country_radio = country_radio;
	}
	public String getPostalCode_radio() {
		return postalCode_radio;
	}
	public void setPostalCode_radio(String postalCode_radio) {
		this.postalCode_radio = postalCode_radio;
	}
	public String getIdType_radio() {
		return idType_radio;
	}
	public void setIdType_radio(String idType_radio) {
		this.idType_radio = idType_radio;
	}
	public String getIdNumber_radio() {
		return idNumber_radio;
	}
	public void setIdNumber_radio(String idNumber_radio) {
		this.idNumber_radio = idNumber_radio;
	}
	public String getLanguageId_radio() {
		return languageId_radio;
	}
	public void setLanguageId_radio(String languageId_radio) {
		this.languageId_radio = languageId_radio;
	}
	public String getIdProofCountry_radio() {
		return idProofCountry_radio;
	}
	public void setIdProofCountry_radio(String idProofCountry_radio) {
		this.idProofCountry_radio = idProofCountry_radio;
	}
	public String getIssueDateIdProof_radio() {
		return issueDateIdProof_radio;
	}
	public void setIssueDateIdProof_radio(String issueDateIdProof_radio) {
		this.issueDateIdProof_radio = issueDateIdProof_radio;
	}
	public String getExpiryDateIdProof_radio() {
		return expiryDateIdProof_radio;
	}
	public void setExpiryDateIdProof_radio(String expiryDateIdProof_radio) {
		this.expiryDateIdProof_radio = expiryDateIdProof_radio;
	}
	public String getIdProofFilePath_radio() {
		return idProofFilePath_radio;
	}
	public void setIdProofFilePath_radio(String idProofFilePath_radio) {
		this.idProofFilePath_radio = idProofFilePath_radio;
	}
	public String getAddressProofType_radio() {
		return addressProofType_radio;
	}
	public void setAddressProofType_radio(String addressProofType_radio) {
		this.addressProofType_radio = addressProofType_radio;
	}
	public String getAddressIssuedBy_radio() {
		return addressIssuedBy_radio;
	}
	public void setAddressIssuedBy_radio(String addressIssuedBy_radio) {
		this.addressIssuedBy_radio = addressIssuedBy_radio;
	}
	public String getIssueDateAddressProof_radio() {
		return issueDateAddressProof_radio;
	}
	public void setIssueDateAddressProof_radio(String issueDateAddressProof_radio) {
		this.issueDateAddressProof_radio = issueDateAddressProof_radio;
	}
	public String getAddressDocLanguage_radio() {
		return addressDocLanguage_radio;
	}
	public void setAddressDocLanguage_radio(String addressDocLanguage_radio) {
		this.addressDocLanguage_radio = addressDocLanguage_radio;
	}
	public String getAddressProofFilePath_radio() {
		return addressProofFilePath_radio;
	}
	public void setAddressProofFilePath_radio(String addressProofFilePath_radio) {
		this.addressProofFilePath_radio = addressProofFilePath_radio;
	}
	
	

}
