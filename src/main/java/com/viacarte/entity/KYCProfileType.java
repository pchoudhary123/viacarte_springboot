package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;



@Table(name="kycprofile_type")
@Entity
public class KYCProfileType extends AbstractEntity<Long> {

    /**
     *
     */
    @Column(name="profile_type_name" ,  nullable = false, unique = true)
    private String profileTypeName;

    @Column(name="monthly_load_limit" ,  nullable = false)
    private double monthlyLoadLimit;

    @Column(name="currency" ,  nullable = false)
    private String currency;

    @Column(name="poi_count" ,  nullable = false)
    private int poiCount; // proof of identity

    @Column(name="poi_definition" ,  nullable = false)
    private String poiDefinition;

    @Column(name="poa_count" ,  nullable = false)
    private int poaCount;

    @Column(name="poa_definition" ,  nullable = false)
    private String poaDefinition;

    @Column(name="status" ,  nullable = false)
    private Status status;


    public String getProfileTypeName() {
        return profileTypeName;
    }

    public void setProfileTypeName(String profileTypeName) {
        this.profileTypeName = profileTypeName;
    }

    public double getMonthlyLoadLimit() {
        return monthlyLoadLimit;
    }

    public void setMonthlyLoadLimit(double monthlyLoadLimit) {
        this.monthlyLoadLimit = monthlyLoadLimit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPoiCount() {
        return poiCount;
    }

    public void setPoiCount(int poiCount) {
        this.poiCount = poiCount;
    }

    public String getPoiDefinition() {
        return poiDefinition;
    }

    public void setPoiDefinition(String poiDefinition) {
        this.poiDefinition = poiDefinition;
    }

    public int getPoaCount() {
        return poaCount;
    }

    public void setPoaCount(int poaCount) {
        this.poaCount = poaCount;
    }

    public String getPoaDefinition() {
        return poaDefinition;
    }

    public void setPoaDefinition(String poaDefinition) {
        this.poaDefinition = poaDefinition;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
