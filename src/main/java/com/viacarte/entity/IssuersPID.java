package com.viacarte.entity;

import com.viacarte.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Table(name = "IssuersPID")
@Entity
public class IssuersPID extends AbstractEntity<Long> {


	@Column(nullable = false, unique = true)
	private String issuerName;
	@Column(nullable = false, unique = true)
	private String pid;
	@Column(nullable = false)
	private String issuerHashId;
	private Status status;

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getIssuerHashId() {
		return issuerHashId;
	}

	public void setIssuerHashId(String issuerHashId) {
		this.issuerHashId = issuerHashId;
	}

}
