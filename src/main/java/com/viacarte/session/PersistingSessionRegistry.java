package com.viacarte.session;

import java.util.List;

import com.viacarte.service.SessionApi;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;


@Service
public class PersistingSessionRegistry implements SessionRegistry,
		ApplicationListener<SessionDestroyedEvent> {


	private SessionApi sessionApi;

	public PersistingSessionRegistry(SessionApi sessionApi) {
		this.sessionApi = sessionApi;
	}

	public void onApplicationEvent(SessionDestroyedEvent event) {
		String sessionId = event.getId();
		removeSessionInformation(sessionId);
	}


	@Override
	public List<Object> getAllPrincipals() {
		return null;
	}

	@Override
	public List<SessionInformation> getAllSessions(Object o, boolean b) {
		return null;
	}

	@Override
	public SessionInformation getSessionInformation(String s) {
		return null;
	}

	@Override
	public  void refreshLastRequest(String sessionId) {
		sessionApi.refreshSession(sessionId);

	}

	@Override
	public void registerNewSession(String s, Object o) {

	}

	@Override
	public void removeSessionInformation(String s) {

	}


}

