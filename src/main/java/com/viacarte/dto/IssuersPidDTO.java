package com.viacarte.dto;

import lombok.Data;

@Data
public class IssuersPidDTO {

    private String issuerName;
    private String pid;
    private String issuerHashId;
}
