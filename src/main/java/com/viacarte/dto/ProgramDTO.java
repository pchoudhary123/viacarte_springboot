package com.viacarte.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ProgramDTO {

    private String programName;
    private String programHashId;
    private MultipartFile termsCondition;
    private String status;
    private String cardModules;




}
