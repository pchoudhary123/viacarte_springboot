package com.viacarte.dto;

import com.viacarte.enums.Status;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;


@Data
public class ClientDetailDTO {

    private String clientHashId;
    private String apiKey;
    private String apiToken;
    private String entityName;
    private String primaryName;
    private String primaryContact;
    private String primaryEmail;
    private String billingName;
    private String billingPhone;
    private String billingEmail;
    private MultipartFile clientLogo;
    private String colorCode;
    private String clientUrlId;
    private String address;
    private String programIds;
    private String country;
}
