package com.viacarte.dto;

import com.viacarte.enums.Status;
import lombok.Data;

@Data
public class KYCProfileTypeDTO {

    private String profileTypeName;
    private double monthlyLoadLimit;
    private String currency;
    private int poiCount; // proof of identity
    private String poiDefinition;
    private int poaCount;
    private String poaDefinition;
    private Status status;
}
