package com.viacarte.dto;

import com.viacarte.enums.Status;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CardArrayDTO {
    private String cardName;
    private String currency;
    private long issuerId;
    private String pid;
    private String programType;
    private Double MaximumBalanceLimit1;
    private Double BalanceLimit1;
    private Double MaximumBalanceLimit2;
    private Double BalanceLimit2;
    private int Limits;
    private MultipartFile cardImage;
    private String header1;
    private String header2;
    private String benifitPoint1;
    private String benifitPoint2;
    private String benifitPoint3;
    private String cardArrayHashId;
    private Status status;
}
