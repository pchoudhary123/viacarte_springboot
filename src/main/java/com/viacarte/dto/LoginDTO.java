package com.viacarte.dto;

import lombok.Data;

@Data
public class LoginDTO {

    private String username;
    private String password;
    private boolean validate;
    private String mobileToken;
    private String androidDeviceID;
    private boolean hasDeviceId;
    private String mPin;
    private String otp;
    private String ipAddress;
    private String dateRange;
    private String locale;
    private String selectCountry;
    private String otpSendBy;



}
