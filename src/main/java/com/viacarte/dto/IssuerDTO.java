package com.viacarte.dto;

import com.viacarte.enums.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class IssuerDTO {

    private String bankName;
    private String brand;
    private String idType;
}
