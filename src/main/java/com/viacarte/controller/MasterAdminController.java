package com.viacarte.controller;

import com.viacarte.dto.*;
import com.viacarte.response.ResponseJson;
import com.viacarte.service.ProcessorAPI;
import com.viacarte.service.SessionApi;
import com.viacarte.service.UserApi;
import com.viacarte.session.PersistingSessionRegistry;
import com.viacarte.validation.LoginValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/Master_Admin")
public class MasterAdminController {

    private final ProcessorAPI processorApi;
    private final SessionApi sessionApi;
    private final PersistingSessionRegistry persistingSessionRegistry;
    private final LoginValidation loginValidation;
    private final UserApi userApi;


    @Autowired
    public MasterAdminController(ProcessorAPI processorApi, SessionApi sessionApi, PersistingSessionRegistry persistingSessionRegistry, LoginValidation loginValidation, UserApi userApi){
        this.processorApi = processorApi;
        this.sessionApi = sessionApi;
        this.persistingSessionRegistry =persistingSessionRegistry;
        this.loginValidation = loginValidation;
        this.userApi = userApi;
    }

    @GetMapping(value = { "/login" })
    public String login(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
        return "login error";
    }

    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/loginPost", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseJson> loginPost(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
        return new ResponseEntity<ResponseJson>( HttpStatus.OK);
    }
    @GetMapping(value = { "/index" })
    public ResponseEntity<ResponseJson> index(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
        return new ResponseEntity<ResponseJson>(sessionApi.registerNewSession(request), HttpStatus.OK);
    }

    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/sendOTP", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> sendOTP(HttpServletRequest request, HttpServletResponse response,
                                              HttpSession session, Model model, @RequestBody LoginDTO loginDTO) {
        return userApi.sendOTP( loginDTO,  request,  response );
    }

    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/validateOTP", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> validateOTP(HttpServletRequest request, HttpServletResponse response,
                              HttpSession session, Model model, @RequestBody LoginDTO loginDTO) {
        return userApi.validateOTP( loginDTO,  request,  response );
    }

    /*********** create kyc profile type *******/
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/createKycProfileType", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson createKycProfileType(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                                  @RequestBody KYCProfileTypeDTO kycProfileTypeDTO, Model model, RedirectAttributes redirectAttributes) {
        return processorApi.createKycProfileType( kycProfileTypeDTO,  request,  response );
    }

    /*********** list kyc profile type *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/kycProfileTypes",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseJson> listKycProfileTypes(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
        return new ResponseEntity<ResponseJson>(processorApi.listKycProfileTypes(request), HttpStatus.OK);
    }


    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/addIssuer",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object addIssuer(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.addIssuer(request);
    }

    /*********** add Issuer *******/
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/addIssuer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson addIssuer(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                  @RequestBody IssuerDTO issuerDTO, Model model, RedirectAttributes redirectAttributes) {
        return processorApi.addIssuer( issuerDTO,  request,  response );
    }
    /*********** list of Issuer *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/listIssuers",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object listIssuers(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getIssuers(request);
    }

    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/addIssuersPID",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object addIssuersPID(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getIssuers(request);
    }

    /*********** add Issuer *******/
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/addIssuersPID", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson addIssuersPID(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                      @RequestBody IssuersPidDTO issuersPidDTO, Model model, RedirectAttributes redirectAttributes) {
        return processorApi.createIssuersPID( issuersPidDTO,  request,  response );
    }

    /*********** list of Issuer PID *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/listIssuersPID",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object listIssuersPID(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getIssuersPID(request);
    }

    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/addCardArray",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object addCardArray(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getIssuersAndKycProfileTypes(request);
    }

    /*********** add Card Array *******/
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/addCardArray", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson addCardArray(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                     @ModelAttribute CardArrayDTO cardArrayDTO, Model model, RedirectAttributes redirectAttributes) throws IOException {
        return processorApi.addCardArray( cardArrayDTO,  request,  response );
    }

    /*********** list of Card Array *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/listCardArray",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object listCardArray(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getCardArray(request);
    }

    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/addProgram",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object addProgram(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getModules(request);
    }

    /*********** add Card Array *******/
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/addProgram", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson addProgram(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                     @ModelAttribute ProgramDTO programDTO, Model model, RedirectAttributes redirectAttributes) throws IOException {
        return processorApi.addProgram( programDTO,  request,  response );
    }

    /*********** list of Card Array *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/getProgramList",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getProgramList(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getProgramList(request);
    }

    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/addClientDetail",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object addClientDetail(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getProgramList(request);
    }

    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*", methods = RequestMethod.POST, origins = {"*"})
    @PostMapping(value = "/addClientDetail", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson addClientDetail(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                                   @ModelAttribute ClientDetailDTO clientDetailDTO, Model model, RedirectAttributes redirectAttributes) throws IOException {
        return processorApi.addClientDetail( clientDetailDTO,  request,  response );
    }

    /*********** list of Card Array *******/
    @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/getClientList",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getClientList(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getClientList(request);
    }

  /*  @CrossOrigin(allowCredentials = "true",allowedHeaders = "*", methods = RequestMethod.GET,origins ={"*"})
    @GetMapping(value="/editClientDetail",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object editClientDetail(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model){
        return processorApi.getProgramList(request);
    }*/
}
